<?php

namespace App\Http\Controllers;

use App\Models\Message;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MessageController extends Controller
{

    // public function __construct()
    // {
    //     // $this->middleware('auth')->only('create', 'store', 'edit', 'update', 'destroy');
    //     $this->middleware('auth')->except('index', 'show');
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Sidebar info
        $totalMessages = Message::where('from_user_id', auth()->user()->id)->orWhere('to_user_id', auth()->user()->id)->count();
        $totalMessagesRecived = Message::where('to_user_id', auth()->user()->id)->count();
        $totalMessagesSended = Message::where('from_user_id', auth()->user()->id)->count();
        $totalMessagesNoReaded = Message::where('readed', false)->where('to_user_id', auth()->user()->id)->count();
        return view('messages.index', [
            'allMessages' => Message::where('from_user_id', auth()->user()->id)->orWhere('to_user_id', auth()->user()->id)->orderby('created_at', 'DESC')->paginate(10),

            // 1 -> Da problemas = Object of class stdClass could not be converted to string
            // 'allMessages' => DB::select(DB::raw('SELECT t2.conversations, t1.* FROM messages t1
            // JOIN (SELECT conversation_id, MAX(id) id, COUNT(conversation_id) as conversations FROM messages GROUP BY conversation_id) t2
            // ON t1.id = t2.id AND t1.conversation_id = t2.conversation_id;') ),

            // 2 -> No da problemas pero no se ve nada
            // 'allMessages' => Message::select(DB::raw("SELECT t2.conversations, t1.* FROM messages as t1
            // JOIN (SELECT conversation_id, MAX(id) as id, COUNT(conversation_id) as conversations FROM messages GROUP BY conversation_id) as t2
            // ON t1.id = t2.id AND t1.conversation_id = t2.conversation_id")),

            // 3 -> Da problemas
            // 'allMessages' => DB::table('messages as t1')->select('t2.conversations AS conversations, t1.*')
            // ->join('(SELECT conversation_id, MAX(id) id, COUNT(conversation_id) as conversations FROM messages GROUP BY conversation_id) as t2', function ($join) {
            //     $join->on('t1.id', '=', 't2.id')->on('t1.conversation_id', '=', 't2.conversation_id');
            // }),

            // $bit_ids = Bid::select(DB::raw('max(id) as id'))
            // ->where('buyer_account_ID', $request->id)
            // ->groupBy('seller_account_ID')
            // ->get(),

            // Corect Query
            // SELECT t1.*, t2.conversations FROM messages t1
            // JOIN (SELECT conversation_id, MAX(id) id, COUNT(conversation_id) conversations FROM messages GROUP BY conversation_id) t2
            // ON t1.id = t2.id AND t1.conversation_id = t2.conversation_id;

            'noReadedMessages' => Message::where('to_user_id', auth()->user()->id)->where('readed', false)->orderby('created_at', 'DESC')->paginate(10),
            'sendedMessages' => Message::where('from_user_id', auth()->user()->id)->orderby('created_at', 'DESC')->paginate(10),
            'recivedMessages' => Message::where('to_user_id', auth()->user()->id)->orderby('created_at', 'DESC')->paginate(10),
            // Sidebar info
            'totalMessages' => $totalMessages,
            'totalMessagesRecived' => $totalMessagesRecived,
            'totalMessagesSended' => $totalMessagesSended,
            'totalMessagesNoReaded' => $totalMessagesNoReaded
        ]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function show(Message $message) {
        if($message->readed == false) {
            $message->readed = true;
            $message->save();
        }

        $from_user = User::find($message->from_user_id);
        $to_user = User::find($message->to_user_id);

        // Sidebar info
        $totalMessages = Message::where('from_user_id', auth()->user()->id)->orWhere('to_user_id', auth()->user()->id)->count();
        $totalMessagesRecived = Message::where('to_user_id', auth()->user()->id)->count();
        $totalMessagesSended = Message::where('from_user_id', auth()->user()->id)->count();
        $totalMessagesNoReaded = Message::where('readed', false)->where('to_user_id', auth()->user()->id)->count();

        return view('messages.show', [
            'message' => $message,
            'from_user' => $from_user,
            'to_user' => $to_user,
            // Sidebar info
            'totalMessages' => $totalMessages,
            'totalMessagesRecived' => $totalMessagesRecived,
            'totalMessagesSended' => $totalMessagesSended,
            'totalMessagesNoReaded' => $totalMessagesNoReaded
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function edit(Message $message)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Message $message)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Message $message)
    {
        $message->delete();
        return redirect()->route('messages.index')->with('status', 'El mensaje fue eliminado con éxito.');
    }

    public function destroyMore(Request $request) {
        $messages = $request->input('messages');
        foreach ($messages as $message => $id) {
            // $message= Message::find($id);
            $message = Message::findOrFail($id);
            $message->delete();
        }
        return back()->with('status','Operation Successful! You are destroy ' + count($messages) + ' messages');
     }


}
