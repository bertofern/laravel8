<?php

namespace App\Http\Controllers;

use App\Mail\MessageContact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function store(Request $request)
    {
        $dataForm = request()->validate([
            'name'=> 'required',
            'email'=> 'required|email',
            'subject'=> 'required',
            'content'=> 'required|min:20'
        ], [
            'name.required'=> __('The fullname field is required.'),
        ]);

        //Cange send for queue
        Mail::to('bertofern@gmail.com')->queue(new MessageContact($dataForm));

        //Probar como se ve el mensaje en el navegador
        // return new MessageContact($dataForm);

        // return 'Mensaje enviado';
        return back()->with('status', 'Recibimos tu mensaje, te responderemos lo antes posible.');
    }
}
