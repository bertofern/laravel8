<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Course;
use App\Models\User;
use App\Http\Requests\SaveCourseRequest;

class CourseController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth')->only('create', 'store', 'edit', 'update', 'destroy');
        $this->middleware('auth')->except('index', 'show');
    }

    public function index()
    {
        // latest() -> default= created_at
        // latest(''updated_at)
        // $courses= Curso::latest()->get();
        // $courses= Curso::latest()->paginate(10);

        // $courses= Course::orderby('created_at', 'DESC')->paginate(10);
        // return view('courses', compact('courses'));

        return view('courses.index', [
            'courses' => Course::orderby('created_at', 'DESC')->paginate(10)
        ]);
    }


    public function create() {
        return view('courses.create');
    }

    public function store(Request $request) {
        //Proteccion contra la asignacion massiva
        $fields = request()->validate([
            'title'=> 'required',
            'slug'=> 'required',
            'description'=> 'required',
        ]);

        Course::create($fields);

        return redirect()->route('courses.index')->with('status', 'El curso fue creado con éxito.');
    }


    // public function show($id) {
    //     return view('courses.show', [
    //         'course' => Course::findOrFail($id)
    //     ]);
    // }

    // Rutas Amigables
    public function show(Course $course) {
        return view('courses.show', [
            'course' => $course
        ]);
    }

    public function edit(Course $course) {
        return view('courses.edit', [
            'course' => $course
        ]);
    }

    public function update(Course $course, SaveCourseRequest $request) {
        $course->update($request->validated());
        return redirect()->route('courses.show', $course)->with('status', 'El curso fue actualizado con éxito.');
    }

    public function destroy(Course $course) {
        $course->delete();
        return redirect()->route('courses.index')->with('status', 'El curso fue eliminado con éxito.');
    }
}
