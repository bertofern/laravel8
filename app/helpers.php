<?php

if (! function_exists('setNavActive')) {
    function setNavActive($routeName) {
        return  request()->routeIs($routeName) ? 'active' : '';
    }
}
