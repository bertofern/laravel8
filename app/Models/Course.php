<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Laravelista\Comments\Commentable;

class Course extends Model
{
    use HasFactory;
    use Commentable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'description',
    ];

    protected $guarded = [
        'id',
        // 'approved',
        'created_at',
        'updated_at',
    ];

    // //URL's amigables cambia la busqueda de ID por la de URL
    public function getRouteKeyName() {
        return 'slug';
    }

}
