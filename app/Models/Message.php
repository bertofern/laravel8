<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    protected $fillable = [
        'subject', 'message',
    ];

    protected $guarded = [
        'id',
        'readed',
        'created_at',
        'updated_at',
    ];
}
