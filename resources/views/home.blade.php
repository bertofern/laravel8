@extends('layouts.app-primary')

@section('title', 'Home')

@section('css-after')
    <style>
        .popover-header {
            color: black;
        }
        #alert {
            position: fixed;
            top: 0;
            right: 0;
            margin: 25px;
        }
    </style>
@endsection

@section('breadcrumb')
<li class="breadcrumb-item active" aria-current="page">
    <a><span class="d-inline-block icon-width oi oi-home"></span> @lang('Home')</a>
</li>
@endsection

@section('content')

<div id="alert" class="alert alert-success alert-dismissible fade show" role="alert">
    <strong>Wellcome</strong> You are in Home!
    <button id="popover-1" type="button" class="btn btn-sm btn-info badge badge-pill" data-bs-toggle="popover" title="Popover title" data-bs-content="And here's some amazing content. It's very engaging. Right?"><i class="fas fa-info"></i></button>
    <a href="services.html" class="alert-link">Entra ya!</a>
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>

{{-- @include('layouts.mini-slider') --}}

@endsection


@section('js-after')
    <script>
        var popover = new bootstrap.Popover(document.querySelector('#popover-1'));
    </script>
@endsection

