<!-- Jumbotron -->
<div class="jumbotron jumbotron-fluid" style="background-image:linear-gradient(to right, #fed136 , #fa944b00),url('images/pages/programmer.jpg');">
    <div class="container">
        <h1 class="display-4 paradox">PARADOX WEB</h1>
        <p class="lead">Paradox Web es una empresa especializada en ofrecer soluciones creativas, tecnológicas y funcionales para cualquier tipo de servicios y aplicaciones online, siempre enfocadas a Internet.</p>
    </div>
</div>
