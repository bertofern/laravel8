<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Google Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet">
    {{-- <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.19.1/css/mdb.min.css" rel="stylesheet"> --}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        #sidebar {
            width: 20%;
            height: 100vh;
            background: #ffffff;
        }
    </style>
</head>

<body>
    {{-- <div class="d-flex">
        <div id="sidebar" class="shadow">
            <div class="list-group list-group-flush">
                <a class="list-group-item list-group-item-action"><i class="fas fa-inbox"></i>  All
                    <span class="badge badge-pill badge-light">{{ $totalMessages }}</span></a>
                <a class="list-group-item list-group-item-action"><i class="fas fa-bullhorn"></i>  No Readed
                    <span class="badge badge-pill badge-dark">{{ $totalMessagesNoReaded }}</span></a>
                <a class="list-group-item list-group-item-action"><i class="fas fa-paper-plane"></i>  Sended
                    <span class="badge badge-pill badge-light">{{ $totalMessagesSended }}</span></a>
                <a class="list-group-item list-group-item-action"><i class="fas fa-envelope-open-text"></i>  Recived
                    <span class="badge badge-pill badge-light">{{ $totalMessagesRecived }}</span></a>
            </div>
        </div>
        <div class="content w-100">
            @include('layouts.nav')
            <main class="py-4">
                @yield('content')
            </main>
        </div>
    </div> --}}


    <div id="app">
        @include('layouts.nav')
        <div class="d-flex mt-1">
            <div id="sidebar" class="shadow">
                <div class="list-group list-group-flush">
                    <br>
                    <a class="btn btn-dark mx-4" href="/messages/create">
                        <i class="fas fa-plus"></i>
                    </a>
                    <br>
                    <a class="list-group-item list-group-item-action" id="btn-all-messages"><i class="fas fa-inbox"></i>  All
                        <span class="badge badge-pill badge-light">{{ $totalMessages }}</span></a>
                    <a class="list-group-item list-group-item-action" id="btn-no-readed-messages"><i class="fas fa-bullhorn"></i>  No Readed
                        <span class="badge badge-pill badge-dark">{{ $totalMessagesNoReaded }}</span></a>
                    <a class="list-group-item list-group-item-action" id="btn-sended-messages"><i class="fas fa-paper-plane"></i>  Sended
                        <span class="badge badge-pill badge-light">{{ $totalMessagesSended }}</span></a>
                    <a class="list-group-item list-group-item-action" id="btn-recived-messages"><i class="fas fa-envelope-open-text"></i>  Recived
                        <span class="badge badge-pill badge-light">{{ $totalMessagesRecived }}</span></a>
                </div>
            </div>
            <div class="content w-100">
                <main class="py-4">
                    @yield('content')
                </main>
            </div>
        </div>

    </div>


    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>


    <script>

    // GRID FUNCTIONS
        $('#btn-all-messages').click(function(){
            $( "#all-messages" ).removeClass( "d-none" ).addClass( "show" );
            $( "#no-readed-messages" ).removeClass( "show" ).addClass( "d-none" );
            $( "#sended-messages" ).removeClass( "show" ).addClass( "d-none" );
            $( "#recived-messages" ).removeClass( "show" ).addClass( "d-none" );
        });
        $('#btn-no-readed-messages').click(function(){
            $( "#no-readed-messages" ).removeClass( "d-none" ).addClass( "show" );
            $( "#all-messages" ).removeClass( "show" ).addClass( "d-none" );
            $( "#sended-messages" ).removeClass( "show" ).addClass( "d-none" );
            $( "#recived-messages" ).removeClass( "show" ).addClass( "d-none" );
        });
        $('#btn-sended-messages').click(function(){
            $( "#sended-messages" ).removeClass( "d-none" ).addClass( "show" );
            $( "#no-readed-messages" ).removeClass( "show" ).addClass( "d-none" );
            $( "#all-messages" ).removeClass( "show" ).addClass( "d-none" );
            $( "#recived-messages" ).removeClass( "show" ).addClass( "d-none" );
        });
        $('#btn-recived-messages').click(function(){
            $( "#recived-messages" ).removeClass( "d-none" ).addClass( "show" );
            $( "#no-readed-messages" ).removeClass( "show" ).addClass( "d-none" );
            $( "#sended-messages" ).removeClass( "show" ).addClass( "d-none" );
            $( "#all-messages" ).removeClass( "show" ).addClass( "d-none" );
        });

    </script>
</body>

</html>
