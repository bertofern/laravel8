<!-- SliderShow carousel -->
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img class="d-block w-100" src="images/slider/1-greece.jpg" alt="First slide">
            <div class="carousel-caption d-none d-md-block">
                <h5> </h5>
                <p> </p>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="images/slider/2-harp.jpg" alt="Second slide">
            <div class="carousel-caption d-none d-md-block">
                <h5> </h5>
                <p> </p>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="images/slider/3-auditory.jpg" alt="Third slide">
            <div class="carousel-caption d-none d-md-block">
                <h5> </h5>
                <p> </p>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="images/slider/4-live.jpg" alt="Second slide">
            <div class="carousel-caption d-none d-md-block">
                <h5> </h5>
                <p> </p>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="images/slider/5-opera.jpg" alt="Third slide">
            <div class="carousel-caption d-none d-md-block">
                <h5> </h5>
                <p> </p>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="images/slider/6-cine.jpg" alt="Second slide">
            <div class="carousel-caption d-none d-md-block">
                <h5> </h5>
                <p> </p>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="images/slider/7-classical.jpg" alt="Third slide">
            <div class="carousel-caption d-none d-md-block">
                <h5> </h5>
                <p> </p>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="images/slider/8-ballet.jpg" alt="Second slide">
            <div class="carousel-caption d-none d-md-block">
                <h5> </h5>
                <p> </p>
            </div>
        </div>
        <div class="carousel-item">
            <img class="d-block w-100" src="images/slider/9-audience.jpg" alt="Third slide">
            <div class="carousel-caption d-none d-md-block">
                <h5> </h5>
                <p> </p>
            </div>
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
