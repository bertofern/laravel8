<div class="container">

    <div class="row">
        <div class=" col-lg-3 col-md-4 col-sm-6 col-6 float-left">
            <figure class="snip1477">
                <img src="{{ asset('images/proyects/cruilla.png')}}" />
                <div class="title">
                    <div>
                        <h2>Editorial</h2>
                        <h4>Cruïlla</h4>
                    </div>
                </div>
                <figcaption>
                    <p>Publicación de literatura infantil y juvenil y materiales escolares en lengua catalana</p>
                </figcaption>
                <a href="https://www.cruilla.cat/" target="_blank"></a>
            </figure>
        </div>
        <div class=" col-lg-3 col-md-4 col-sm-6 col-6 float-left">
            <figure class="snip1477">
                <img src="{{ asset('images/proyects/restaurantmediterrania.png')}}" />
                <div class="title">
                    <div>
                        <h2>Restaurant</h2>
                        <h4>Mediterrania</h4>
                    </div>
                </div>
                <figcaption>
                    <p>Mediterrania Restaurant & Banquets - Sant Fost de Campsentelles, Barcelona</p>
                </figcaption>
                <a href="https://www.restaurantmediterrania.com/" target="_blank"></a>
            </figure>
        </div>
        <div class=" col-lg-3 col-md-4 col-sm-6 col-6 float-left">
            <figure class="snip1477">
                <img src="{{ asset('images/proyects/entrecomes.png')}}" />
                <div class="title">
                    <div>
                        <h2>Editorial</h2>
                        <h4>Entrecomes</h4>
                    </div>
                </div>
                <figcaption>
                    <p>Librería online de la Editorial Entrecomes - Barcelona</p>
                </figcaption>
                <a href="https://www.editorial-entrecomes.com/" target="_blank"></a>
            </figure>
        </div>
        <div class=" col-lg-3 col-md-4 col-sm-6 col-6 float-left">
            <figure class="snip1477">
                <img src="{{ asset('images/proyects/aineoserveis.png')}}" />
                <div class="title">
                    <div>
                        <h2>Asesoria Fiscal</h2>
                        <h4>Aineo Serveis</h4>
                    </div>
                </div>
                <figcaption>
                    <p>Aineo Serveis - Asesoria Fiscal - Granollers</p>
                </figcaption>
                <a href="https://www.aineoserveis.com/" target="_blank"></a>
            </figure>
        </div>
        <div class=" col-lg-3 col-md-4 col-sm-6 col-6 float-left">
            <figure class="snip1477">
                <img src="{{ asset('images/proyects/albepet.png')}}" />
                <div class="title">
                    <div>
                        <h2>Animales exóticos</h2>
                        <h4>Albepet</h4>
                    </div>
                </div>
                <figcaption>
                    <p>Importación, exportación y comercialización de animales exóticos - Getafe, Madrid</p>
                </figcaption>
                <a href="https://albepet.com/" target="_blank"></a>
            </figure>
        </div>
        <div class=" col-lg-3 col-md-4 col-sm-6 col-6 float-left">
            <figure class="snip1477">
                <img src="{{ asset('images/proyects/cardenes.png')}}" />
                <div class="title">
                    <div>
                        <h2>Farmacia</h2>
                        <h4>Cárdenes</h4>
                    </div>
                </div>
                <figcaption>
                    <p>Farmacia & Parafarmacia Cárdenes Altavista - Las Palmas de Gran Canaria, Las Palmas</p>
                </figcaption>
                <a href="https://www.cardenessalud.com/" target="_blank"></a>
            </figure>
        </div>
        <div class=" col-lg-3 col-md-4 col-sm-6 col-6 float-left">
            <figure class="snip1477">
                <img src="{{ asset('images/proyects/feriche.png')}}" />
                <div class="title">
                    <div>
                        <h2>Farmacia</h2>
                        <h4>Feriche</h4>
                    </div>
                </div>
                <figcaption>
                    <p>Farmacia Feriche. Farmacia en Barcelona</p>
                </figcaption>
                <a href="https://www.farmaciaferiche.com/" target="_blank"></a>
            </figure>
        </div>
        <div class=" col-lg-3 col-md-4 col-sm-6 col-6 float-left">
            <figure class="snip1477">
                <img src="{{ asset('images/proyects/yangüela.png')}}" />
                <div class="title">
                    <div>
                        <h2>Farmàcia</h2>
                        <h4>Yangüela</h4>
                    </div>
                </div>
                <figcaption>
                    <p>Farmàcia Yangüela. Farmacia en Castellar del Vallés, Barcelona</p>
                </figcaption>
                <a href="https://farmaciayanguela.com/" target="_blank"></a>
            </figure>
        </div>
        <div class=" col-lg-3 col-md-4 col-sm-6 col-6 float-left">
            <figure class="snip1477">
                <img src="{{ asset('images/proyects/taxfarma.png')}}" />
                <div class="title">
                    <div>
                        <h2>Asesor fiscal</h2>
                        <h4>TAXFARMA</h4>
                    </div>
                </div>
                <figcaption>
                    <p>Asesoría y gestoría especializada en Farmacias. Planificación fiscal y tributaria. Consultoría. Sant Cugat del Vallès, Barcelona</p>
                </figcaption>
                <a href="https://www.taxfarma.com/" target="_blank"></a>
            </figure>
        </div>
        <div class=" col-lg-3 col-md-4 col-sm-6 col-6 float-left">
            <figure class="snip1477">
                <img src="{{ asset('images/proyects/BASF2.png')}}" />
                <div class="title">
                    <div>
                        <h2>Química.</h2>
                        <h4>BASF</h4>
                    </div>
                </div>
                <figcaption>
                    <p>BASF Española. Empresa química Alemana</p>
                </figcaption>
                <a href="https://www.basf.com/es/" target="_blank"></a>
            </figure>
        </div>
        <div class=" col-lg-3 col-md-4 col-sm-6 col-6 float-left">
            <figure class="snip1477">
                <img src="{{ asset('images/proyects/bruguer.png')}}" />
                <div class="title">
                    <div>
                        <h2>Pinturas</h2>
                        <h4>Bruguer</h4>
                    </div>
                </div>
                <figcaption>
                    <p>Bruguer marca referencial de pinturas de AkzoNobel</p>
                </figcaption>
                <a href="https://www.bruguer.es/" target="_blank"></a>
            </figure>
        </div>
        <div class=" col-lg-3 col-md-4 col-sm-6 col-6 float-left">
            <figure class="snip1477">
                <img src="{{ asset('images/proyects/colacao.png')}}" />
                <div class="title">
                    <div>
                        <h2>Productos</h2>
                        <h4>Colacao</h4>
                    </div>
                </div>
                <figcaption>
                    <p>Cola Cao. Marca y producto fabricado en Barcelona por Idilia Foods</p>
                </figcaption>
                <a href="https://colacao.es/"  target="_blank"></a>
            </figure>
        </div>
        <div class=" col-lg-3 col-md-4 col-sm-6 col-6 float-left">
            <figure class="snip1477">
                <img src="{{ asset('images/proyects/danone.png')}}" />
                <div class="title">
                    <div>
                        <h2>Alimentación</h2>
                        <h4>Danone</h4>
                    </div>
                </div>
                <figcaption>
                    <p>Danone. Multinacional agroalimentaria francesa</p>
                </figcaption>
                <a href="https://www.danone.es/"  target="_blank"></a>
            </figure>
        </div>
        <div class=" col-lg-3 col-md-4 col-sm-6 col-6 float-left">
            <figure class="snip1477">
                <img src="{{ asset('images/proyects/weekly-villas2.png')}}" />
                <div class="title">
                    <div>
                        <h2>Rent holiday villas</h2>
                        <h4>Weekly Villas</h4>
                    </div>
                </div>
                <figcaption>
                    <p>Agencia de alquiler de alojamientos para vacaciones en Sitges, Tarragona</p>
                </figcaption>
                <a href="https://www.weeklyvillas.es/"  target="_blank"></a>
            </figure>
        </div>
        <div class=" col-lg-3 col-md-4 col-sm-6 col-6 float-left">
            <figure class="snip1477">
                <img src="{{ asset('images/proyects/lindt2.png')}}" />
                <div class="title">
                    <div>
                        <h2>Chocolatería</h2>
                        <h4>Lindt</h4>
                    </div>
                </div>
                <figcaption>
                    <p>Lindt & Sprüngli. Compañía suiza especializada en chocolatería de lujo.</p>
                </figcaption>
                <a href="https://www.lindt.es/"  target="_blank"></a>
            </figure>
        </div>
        <div class=" col-lg-3 col-md-4 col-sm-6 col-6 float-left">
            <figure class="snip1477">
                <img src="{{ asset('images/proyects/velarte.png')}}" />
                <div class="title">
                    <div>
                        <h2>Productos</h2>
                        <h4>Snackium</h4>
                    </div>
                </div>
                <figcaption>
                    <p>Proveedor mayorista de alimentos</p>
                </figcaption>
                <a href="https://www.velarte.com/"  target="_blank"></a>
            </figure>
        </div>

    </div>

</div>
