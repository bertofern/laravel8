<!-- Footer -->
<footer class="footer">
    <div class="footer-middle">
        <div class="container">
            <div class="row pb-2">
                <div class="col-md-3 col-sm-6">
                    <div class="footer-pad text-center">
                        <address>
                            <h4 class="h4">PARADOX WEB</h4>
                            <span class="oi oi-location"></span> Figueres, GI 17600<br />
                            <abbr title="Phone"></abbr><span class="oi oi-phone"></span> +34 697321944<br>
                            <span class="oi oi-envelope-closed"></span><a href="mailto:info@paradox-web.com"> info@paradox-web.com</a>
                        </address>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="footer-pad text-center">
                        <h4 class="h4">Heading 1</h4>
                        <ul class="list-unstyled">
                            <li><a href="#!">Información</a></li>
                            <li><a href="#!">Aviso Legal</a></li>
                            <li><a href="#!">Contacto</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="footer-pad text-center">
                        <h4 class="h4">Heading 2</h4>
                        <ul class="list-unstyled">
                            <li><a href="#!">Link 1</a></li>
                            <li><a href="#!">Link 2</a></li>
                            <li><a href="#!">Link 3</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3  col-sm-6 text-center" style="padding-bottom: 10px;">
                    <h4 class="h4">Follow Us</h4>
                    <a href="#">
                        <i class="fab fa-twitter fa-lg"></i>
                    </a>&nbsp;
                    <a href="#">
                        <i class="fab fa-facebook fa-lg"></i>
                    </a>&nbsp;
                    <a href="#">
                        <i class="fab fa-instagram fa-lg"></i>
                    </a>&nbsp;
                    <a href="#">
                        <i class="fab fa-linkedin fa-lg"></i>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 copy">
                    <p class="text-center">&copy; Copyright 2021 - <a href="https://paradox-web.com">
                        paradox-web.com</a> All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- Scroll to Top Button-->
<a onclick="topFunction()" id="btnToTop" title="Go to top"><i class="fas fa-angle-up"></i></a>


