<div class="col-lg-12 text-center text-black">
    <h2 id="text1" class="section-heading content-center text-uppercase">Desarrollo de Aplicaciones
        Web<span>&#160;</span></h2>
    <h3 class="section-subheading text-muted"></h3>
    <h2>Filter DIV Elements</h2>

    <div id="filter-container">
        <button class="btn-f active" onclick="filterSelection('all')"> Todos</button>
        <button class="btn-f" onclick="filterSelection('cars')"> Cars</button>
        <button class="btn-f" onclick="filterSelection('animals')"> Animals</button>
        <button class="btn-f" onclick="filterSelection('fruits')"> Fruits</button>
        <button class="btn-f" onclick="filterSelection('colors')"> Colors</button>
    </div>

    <div class="container-a contect-center">
        <div class="filterDiv cars">BMW</div>
        <div class="filterDiv colors fruits">Orange</div>
        <div class="filterDiv cars">Volvo</div>
        <div class="filterDiv colors">Red</div>
        <div class="filterDiv cars animals">Mustang</div>
        <div class="filterDiv colors">Blue</div>
        <div class="filterDiv animals">Cat</div>
        <div class="filterDiv animals">Dog</div>
        <div class="filterDiv fruits">Melon</div>
        <div class="filterDiv fruits animals">Kiwi</div>
        <div class="filterDiv fruits">Banana</div>
        <div class="filterDiv fruits">Lemon</div>
        <div class="filterDiv animals">Cow</div>
    </div>
</div>
