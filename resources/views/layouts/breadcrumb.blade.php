<!-- Breadcrumb -->
<nav aria-label="breadcrumb">
    <div class="container">
        <ol class="breadcrumb">
            @yield('breadcrumb')
        </ol>
    </div>
</nav>
