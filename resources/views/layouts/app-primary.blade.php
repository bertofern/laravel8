<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }} - @yield('title', 'home')</title>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css" rel="stylesheet" />

    <!-- Open Iconic -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/open-iconic/1.1.1/font/css/open-iconic.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/open-iconic/1.1.1/font/css/open-iconic-bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/open-iconic/1.1.1/font/css/open-iconic-foundation.min.css">

    <!-- Bootstrap core CSS -->
    {{-- <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css" rel="stylesheet"> --}}
    {{-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> --}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Dela+Gothic+One&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Cinzel&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@100&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Goldman&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Codystar:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@600&display=swap" rel="stylesheet">

    <!-- Leafletjs.com -->
    <link rel='stylesheet' href='https://unpkg.com/leaflet@1.7.1/dist/leaflet.css' integrity='sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A==' crossorigin=''>

    <!-- Icon-Flag -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/3.5.0/css/flag-icon.min.css" integrity="sha512-Cv93isQdFwaKBV+Z4X8kaVBYWHST58Xb/jVOcV9aRsGSArZsgAnFIhMpDoMDcFNoUtday1hdjn0nGp3+KZyyFw==" crossorigin="anonymous" />

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        /* Fonts */
        @font-face {
            font-family: myRufan; src: url(../fonts/Rutan/rutan-demibold.otf);
        }

        /* Fonts
        font-family: 'Cinzel', serif;
        font-family: 'Roboto Mono', monospace;
        font-family: 'Nunito', sans-serif;
        font-family: 'Dela Gothic One', cursive;
        font-family: 'Goldman', cursive;
        font-family: 'Codystar', cursive;
        font-family: 'Montserrat', sans-serif;
        */
        .paradox { font-family: myRufan; }
        .font-paradox {
            font-family: 'Montserrat', sans-serif;
        }
        .font-paradox-2 {
            font-family: 'Roboto Mono', monospace;
            vertical-align:sub;
        }

        body {
            width: 100%;
            /* background: #000000; */
            /* color: white; */
            background-repeat: repeat;
            background-image:url('images/pages/organic-line-seamless-pattern-irregular-260nw-1880231914.png');
            background-size: 10%;
        }

        /* Menu */
        #colapsed-menu {
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(46, 46, 46, 0.5);
            position: fixed;
            z-index:3;
        }
        #colapsed-menu-close {
            top: 0;
            right: 0;
            position: fixed;
            z-index:4;
            margin-right: 20px;
            margin-top: 20px;
        }
        .item-menu {
            height: 10%;
        }

        .image-menu-1{
            background-image:url('images/menu/coworking1.jpg');
            background-size: cover;
            background-position: center;
            opacity: 0.3;
        }
        .image-menu-2{
            background-image:url('images/menu/coworking2.jpg');
            background-size: cover;
            background-position: center;
            opacity: 0.3;
        }
        .image-menu-3{
            background-image:url('images/menu/coworking3.jpg');
            background-size: cover;
            background-position: center;
            opacity: 0.3;
        }
        .image-menu-4{
            background-image:url('images/menu/coworking4.jpg');
            background-size: cover;
            background-position: center;
            opacity: 0.3;
        }


        #background-lines, .background-lines{
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(5, 6, 16);
            position: fixed;
            z-index:2;
        }
        .background-lines{
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-color: rgba(5, 6, 16);
            position: fixed;
            z-index:1;
        }
        .line-style{
            border-bottom:2px solid rgb(100, 100, 100);
            z-index:-1;
            left: 0px;
            width:100%;
            height:20%;
        }
        .line-style.right{
            left:auto;
            right:0px;
        }
        #line1{
            /* top: 0.8em; */
            -webkit-animation: line-efect 10s infinite;
            -moz-animation:    line-efect 10s infinite;
            -o-animation:      line-efect 10s infinite;
            animation:         line-efect 10s infinite;
        }
        #line2{
            /* top:1.8em; */
            /*right:15px;*/
            -webkit-animation: line-efect 8s infinite;
            -moz-animation:    line-efect 8s infinite;
            -o-animation:      line-efect 8s infinite;
            animation:         line-efect 8s infinite;
        }
        #line3{
            /* top:2.8em; */
            /* right:15px; */
            /* border-color:#b7bf33; */
            -webkit-animation: line-efect 6s infinite;
            -moz-animation:    line-efect 6s infinite;
            -o-animation:      line-efect 6s infinite;
            animation:         line-efect 6s infinite;
        }
        #line4{
            /* top:3.8em; */
            /* border-color:#b23836; */
            -webkit-animation: line-efect 12s infinite;
            -moz-animation:    line-efect 12s infinite;
            -o-animation:      line-efect 12s infinite;
            animation:         line-efect 12s infinite;
        }
        #line5{
            /* top:4.8em; */
            /* border-color:#3953a4; */
            -webkit-animation: line-efect 16s infinite;
            -moz-animation:    line-efect 16s infinite;
            -o-animation:      line-efect 16s infinite;
            animation:         line-efect 16s infinite;
        }
        #line6{
            /* top:5.8em; */
            -webkit-animation: line-efect 6s infinite;
            -moz-animation:    line-efect 6s infinite;
            -o-animation:      line-efect 6s infinite;
            animation:         line-efect 6s infinite;
        }

        @-webkit-keyframes line-efect {
            0%   { width: 100%; }
            50% { width: 100%; }
            80% { width: 0px;}
            100%{ width:100%;}
        }
        @-moz-keyframes line-efect {
            0%   { width: 100%; }
            50% { width: 100%; }
            80% { width: 0px;}
            100%{ width:100%;}
        }
        @-o-keyframes line-efect {
            0%   { width: 100%; }
            50% { width: 100%; }
            80% { width: 0px;}
            100%{ width:100%;}
        }
        @keyframes line-efect {
            0%   { width: 100%; }
            50% { width: 100%; }
            80% { width: 0px;}
            100%{ width:100%;}
        }
    </style>

    @yield('css-after')

</head>
<body>
    <div id="app">
        @include('layouts.nav')
        @if (auth()->check())
            @if (auth()->user()->hasRoles(['super-admin','admin']))
                @include('layouts.nav-admin')
            @endif
        @endif

        @include('layouts.jumbotron')
        @include('layouts.breadcrumb')

        @if (session('status'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('status') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

        <main class="py-4">
            @yield('content')
        </main>

        @include('layouts.footer')
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalMenu" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-fullscreen">
            <div class="modal-content">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    ...
                </div>
            </div>
        </div>
    </div>

    <!-- Menu Hidden 2 -->
    <div id="colapsed-menu" class="hidden">
        <button id="colapsed-menu-close" type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
        <div class="m-4 row" style="height: 100%">
            <div class="col-12 col-sm-12 col-md-5 col-lg-5">
                <ul class="navbar-nav d-flex justify-content-center" style="height: 100%">
                    <li class="nav-item item-menu d-flex align-items-center {{ setNavActive('home') }}">
                        <a class="nav-link d-flex align-items-center" href="{{ route('home') }}" onmouseover="showImg('home')" onmouseout="showImg()">
                            <span class="h6 font-paradox-2">01</span>
                            <span class="h2 ml-4 font-paradox">@lang('Home')</span>
                        </a>
                    </li>
                    <li class="nav-item item-menu d-flex align-items-center {{ setNavActive('about') }}">
                        <a class="nav-link d-flex align-items-center" href="{{ route('about') }} " onmouseover="showImg('about')" onmouseout="showImg()">
                            <span class="h6 font-paradox-2">02</span>
                            <span class="h2 ml-4 font-paradox">@lang('About')</span>
                        </a>
                    </li>
                    <li class="nav-item item-menu d-flex align-items-center {{ setNavActive('our-services') }}">
                        <a class="nav-link d-flex align-items-center" onmouseover="showImg('about')" onmouseout="showImg()">
                            <span class="h6 font-paradox-2">03</span>
                            <span class="h2 ml-4 font-paradox">@lang('Our Services')</span>
                        </a>
                    </li>
                    <li class="nav-item item-menu d-flex align-items-center {{ setNavActive('our-work') }}">
                        <a class="nav-link d-flex align-items-center" onmouseover="showImg('about')" onmouseout="showImg()">
                            <span class="h6 font-paradox-2">04</span>
                            <span class="h2 ml-4 font-paradox">@lang('Our Work')</span>
                        </a>
                    </li>
                    <li class="nav-item item-menu d-flex align-items-center {{ setNavActive('clients') }}">
                        <a class="nav-link d-flex align-items-center" onmouseover="showImg('about')" onmouseout="showImg()">
                            <span class="h6 font-paradox-2">05</span>
                            <span class="h2 ml-4 font-paradox">@lang('Clients')</span>
                        </a>
                    </li>
                    <li class="nav-item item-menu d-flex align-items-center {{ setNavActive('blog') }}">
                        <a class="nav-link d-flex align-items-center" onmouseover="showImg('about')" onmouseout="showImg()">
                            <span class="h6 font-paradox-2">06</span>
                            <span class="h2 ml-4 font-paradox">@lang('Blog')</span>
                        </a>
                    </li>
                    <li class="nav-item {{ setNavActive('contact') }}">
                        <a class="nav-link d-flex align-items-center" href="{{ route('contact') }}" onmouseover="showImg('contact')" onmouseout="showImg()">
                            <span class="h6 font-paradox-2">07</span>
                            <span class="h2 ml-4 font-paradox">@lang('Contact')</span>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="col-0 col-sm-0 col-md-7 col-lg-7">
                <div class="d-flex justify-content-center" style="height: 100%">
                    <div class="d-flex align-items-center">
                        <div class="row" style="margin: 100px; color:rgb(100, 100, 100)">
                            <div class="col-12">
                                <h4 class="h4 paradox text-white">PARADOX WEB</h4>
                            </div>
                            <div class="col-12 mb-3">
                                <i class="fas fa-map-marked-alt fa-2x"></i><span class="ml-4 font-paradox ">Figueres, GI 17600, Spain</span><br />
                            </div>
                            <div class="col-6">
                                <i class="fas fa-mobile-alt fa-2x"></i><span class="ml-4 font-paradox">+34 697321944</span><br>
                            </div>
                            <div class="col-6">
                                <i class="fas fa-envelope-open-text fa-2x"></i><span class="ml-4 font-paradox"><a href="mailto:bertofern@gmail.com">bertofern@gmail.com</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- Decoration Lines Animation -for Menu  --}}
    <div class="background-lines hidden">
        <div id="background-lines">
            <div id="line1" class="line-style"></div>
            <div id="line2" class="line-style"></div>
            <div id="line3" class="line-style right"></div>
            <div id="line4" class="line-style"></div>
            <div id="line5" class="line-style right"></div>
            <div id="line6" class="line-style"></div>
        </div>
    </div>

    <!-- Bootstrap -->
    {{-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> --}}
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>

    <!-- MDB -->
    {{-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/3.0.0/mdb.min.js"></script> --}}
    <!-- Bootstrap Datatables -->
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>

    <!-- leafletjs -->
    <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js" integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA==" crossorigin=""></script>
    <script src='{{ asset('js/leafletjs.js') }}' type="text/javascript"></script>

    <!-- Efects scroll -->
    <script src="https://unpkg.com/scrollreveal"></script>
    <!-- Efects scroll & Scroll to top-->
    <script type="text/javascript" src="{{ asset('js/scroll.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/multiple-slider3.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/filter-div.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#table-users').DataTable();
        } );

        /* Projects JS */
        $(".hover").mouseleave(
            function () {
            $(this).removeClass("hover");
            }
        );

        // Modal Menu
        var myModal = document.getElementById('modalMenu')
        var myInput = document.getElementById('buttonModalMenu')

        myModal.addEventListener('shown.bs.modal', function () {
            myInput.focus()
        });

        // Image mouseover on Menu
        function showImg(menu) {
            var imageBox = document.getElementById("background-lines");
            switch(menu) {
                case 'home':
                    imageBox.classList.add("image-menu-1");
                    break;
                case 'about':
                    imageBox.classList.add("image-menu-2");
                    break;
                case 'courses':
                    imageBox.classList.add("image-menu-3");
                    break;
                case 'contact':
                    imageBox.classList.add("image-menu-4");
                    break;
                default:
                    imageBox.classList.remove("image-menu-1");
                    imageBox.classList.remove("image-menu-2");
                    imageBox.classList.remove("image-menu-3");
                    imageBox.classList.remove("image-menu-4");
            }

        }

    </script>

    @yield('js-after')


</body>
</html>


