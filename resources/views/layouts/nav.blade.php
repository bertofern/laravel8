{{-- <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm"> --}}
<nav id="page-top" class="navbar navbar-expand-lg navbar-dark shadow-sm bg-light nav-style">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="images/pages/chicken.gif" class="d-inline-block align-top" style="width: auto;height:30px">
        </a>
        <a class="navbar-brand d-flex align-items-center" href="{{ url('/') }}">
            <span class="pt-2 paradox">PARADOX WEB</span>
        </a>
        {{-- <a class="navbar-brand" href="{{ url('/') }}">
            {{ config('app.name', 'Laravel') }}
        </a> --}}

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">

                {{-- Alternativa larga de navegación --}}
                {{-- <li class="nav-item {{ request()->routeIs('home') ? 'active' : ''}}"> --}}

                {{-- Hay 2  Maneras de llamar al traductor
                @lang('Home')
                {{ __('Home') }} --}}

                {{-- Con la ayuda de setActive en app/helpers.php --}}
                <li class="nav-item {{ setNavActive('home') }}">
                    <a class="nav-link" href="{{ route('home') }}">@lang('Home')</a>
                </li>
                <li class="nav-item {{ setNavActive('about') }}">
                    <a class="nav-link" href="{{ route('about') }}">@lang('About')</a>
                </li>
                <li class="nav-item {{ setNavActive('courses.*') }}">
                    <a class="nav-link" href="{{ route('courses.index') }}">@lang('Courses')</a>
                </li>
                @if (auth()->check())
                    <li class="nav-item {{ setNavActive('messages.*') }}">
                        <a class="nav-link" href="{{ route('messages.index') }}">@lang('Messages')</a>
                    </li>
                    {{-- @if (auth()->user()->role === 'admin') --}}
                    @if (auth()->user()->hasRoles(['super-admin','admin']))

                        <li class="nav-item {{ setNavActive('users.*') }}">
                            <a class="nav-link" href="{{ route('users.index') }}">@lang('Users')</a>
                        </li>
                    @endif
                @endif
                <li class="nav-item {{ setNavActive('contact') }}">
                    <a class="nav-link" href="{{ route('contact') }}">@lang('Contact')</a>
                </li>
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item dropdown">
                    <a class="nav-link" id="langSelect" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if ( Config::get('app.locale') == 'en')
                            <span class="flag-icon flag-icon-gb"></span>
                        @elseif ( Config::get('app.locale') == 'es' )
                            <span class="flag-icon flag-icon-es"></span>
                        @elseif ( Config::get('app.locale') == 'ca' )
                            <span class="flag-icon flag-icon-es-ca"></span>
                        @endif
                    </a>
                    <div class="dropdown-menu" aria-labelledby="langSelect">
                        <a class="dropdown-item" href="{{ route('language','es') }}">
                            <span class="flag-icon flag-icon-es"></span> ES
                        </a>
                        <a class="dropdown-item" href="{{ route('language','ca') }}">
                            <span class="flag-icon flag-icon-es-ca"></span> CA
                        </a>
                        <a class="dropdown-item" href="{{ route('language','en') }}">
                            <span class="flag-icon flag-icon-gb"></span> EN
                        </a>
                    </div>
                </li>
                <!-- Button modal menu -->
                <li class="nav-item">
                    <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#modalMenu">Menu</button>
                </li>
                {{-- <li class="nav-item dropdown">
                    <a class="nav-link" id="dropdownMessages" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="far fa-bell fa-lg"></i><small class="badge badge-pill badge-danger align-top">9</small>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMessages">
                        <button class="dropdown-item" type="button">Action</button>
                        <button class="dropdown-item" type="button">Another action</button>
                        <button class="dropdown-item" type="button">Something else here</button>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link"><i class="fas fa-moon fa-lg"></i></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link"><i class="fas fa-adjust fa-lg"></i></a>
                </li> --}}

                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                        </li>
                    @endif
                @else

                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                @endguest
            </ul>

        </div>
    </div>
</nav>
