{{-- <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm"> --}}
<nav class="navbar navbar-expand-lg navbar-dark shadow-sm bg-secondary">
    <div class="container">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                {{-- Con la ayuda de setActive en app/helpers.php --}}
                <li class="nav-item {{ setNavActive('courses.*') }}">
                    <a class="nav-link text-center" href="{{ route('courses.index') }}">
                        <i class="fas fa-school"></i><br><span>@lang('Schools')</span>
                    </a>
                </li>
                <li class="nav-item {{ setNavActive('courses.*') }}">
                    <a class="nav-link text-center" href="{{ route('courses.index') }}">
                        <i class="fas fa-chalkboard-teacher"></i><br><span>@lang('Teachers')</span>
                    </a>
                </li>
                <li class="nav-item {{ setNavActive('courses.*') }}">
                    <a class="nav-link text-center" href="{{ route('courses.index') }}">
                        <i class="fas fa-graduation-cap"></i><br><span>@lang('Certificates')</span>
                    </a>
                </li>
                <li class="nav-item {{ setNavActive('courses.*') }}">
                    <a class="nav-link text-center" href="{{ route('courses.index') }}">
                        <i class="fas fa-certificate"></i><br><span>@lang('Certificates')</span>
                    </a>
                </li>
                <li class="nav-item {{ setNavActive('courses.*') }}">
                    <a class="nav-link text-center" href="{{ route('courses.index') }}">
                        <i class="fab fa-leanpub"></i><br><span>@lang('Courses')</span>
                    </a>
                </li>
                <li class="nav-item {{ setNavActive('messages.*') }}">
                    <a class="nav-link text-center" href="{{ route('messages.index') }}">
                        <i class="far fa-comment-alt"></i><br><span>@lang('Messages')</span>
                    </a>
                </li>
                <li class="nav-item {{ setNavActive('users.*') }}">
                    <a class="nav-link text-center" href="{{ route('users.index') }}">
                        <i class="fas fa-users"></i><br><span>@lang('Users')</span>
                    </a>
                </li>
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"> </li>
            </ul>

        </div>
    </div>
</nav>
