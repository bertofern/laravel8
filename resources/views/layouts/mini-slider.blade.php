<!-- Bootstrap 4 Multiple Item Carousel - Advances all 4 slides each time https://www.codeply.com/go/HDu8lT7NxJ -->
<!-- ... -->
<!-- Bootstrap 4 Multiple Item Carousel - Advances one slide at a time https://www.codeply.com/go/3EQkUOhhZz -->
<!-- Configurations: multiple-slider3.css i multiple-slider3.js-->
<div id="recipeCarousel" class="carousel slide w-100" data-ride="carousel">
    <div class="carousel-inner w-100" role="listbox">
        <div class="carousel-item active">
            <img class="d-block col-lg-3 col-md-4 col-sm-6 col-12 img-fluid" src="images/mini-slider/1.jpg">
        </div>
        <div class="carousel-item">
            <img class="d-block col-lg-3 col-md-4 col-sm-6 col-12 img-fluid" src="images/mini-slider/2.jpg">
        </div>
        <div class="carousel-item">
            <img class="d-block col-lg-3 col-md-4 col-sm-6 col-12 img-fluid" src="images/mini-slider/3.jpg">
        </div>
        <div class="carousel-item">
            <img class="d-block col-lg-3 col-md-4 col-sm-6 col-12 img-fluid" src="images/mini-slider/4.jpg">
        </div>
        <div class="carousel-item">
            <img class="d-block col-lg-3 col-md-4 col-sm-6 col-12 img-fluid" src="images/mini-slider/5.jpg">
        </div>
        <div class="carousel-item">
            <img class="d-block col-lg-3 col-md-4 col-sm-6 col-12 img-fluid" src="images/mini-slider/6.jpg">
        </div>
        <div class="carousel-item">
            <img class="d-block col-lg-3 col-md-4 col-sm-6 col-12 img-fluid" src="images/mini-slider/7.jpg">
        </div>
    </div>
    <a class="carousel-control-prev" href="#recipeCarousel" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#recipeCarousel" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
