@extends('layouts.app-primary')

@section('title', trans('Contact'))

@section('breadcrumb')
<li class="breadcrumb-item" aria-current="page">
    <span class="d-inline-block icon-width oi oi-home"></span><a href="{{ url('/') }}">&nbsp;@lang('Home')</a>
</li>
<li class="breadcrumb-item active" aria-current="page"><a>@lang('Contact')</a></li>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12 col-sm-12 col-lg-6 p-2">
            <div class="text-paradox border shadow rounded info-left">
                <div class="container p-5">
                    <h1 class="display-4 text-center">Formulario</h1>
                    <h1 class="display-4 text-center">de <span class="paradox">contacto</span></h1><br>
                    <p class="lead">Si necesitas cualquier tipo de información, contacta con nosotros y te ayudaremos en todo lo posible.</p>
                    <p class="lead">info@paradox-web.com</p>
                    <p class="lead">+34 697 32 19 44</p>
                </div>
            </div><br>
        </div>
        <div class="col-12 col-sm-12 col-lg-6 text-black p-2">
            {{-- <div class="shadow rounded" style="background-image:url('images/pages/stylized-height-topographic-map-contour-260nw-754709692.png');"> --}}
            <div class="border shadow rounded bg-white info-right">
                <div class="container p-4">
                    @if(session('status'))
                        {{ session('status') }}
                    @else
                        <br>
                        <form method="POST" action="/contact" >
                            @csrf
                            <div class="row">
                                <div class="col form-group">
                                    <label for="name">Nombre</label>
                                    <input class="form-control bg-light shadow-sm @error('name') is-invalid @else border-1 @enderror"
                                    name="name" placeholder="Nombre..." value="{{ old('name') }}">
                                    @error('name')
                                        <span class="invalid-feedback">
                                            <small>{{ $message }}</small>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col form-group">
                                    <label for="email">Email</label>
                                    <input class="form-control bg-light shadow-sm @error('email') is-invalid @else border-1 @enderror"
                                    name="email" placeholder="Email..." value="{{ old('email') }}">
                                    @error('email')
                                        <span class="invalid-feedback">
                                            <small>{{ $message }}</small>
                                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="subject">Asunto</label>
                                <input class="form-control bg-light shadow-sm @error('subject') is-invalid @else border-1 @enderror"
                                name="subject" placeholder="Asunto..." value="{{ old('subject') }}">
                                @error('subject')
                                    <span class="invalid-feedback">
                                        <small>{{ $message }}</small>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="content">Mensaje</label>
                                <textarea class="form-control bg-light shadow-sm @error('content') is-invalid @else border-1 @enderror"
                                name="content" id="" cols="30" rows="10">{{ old('content') }}</textarea>
                                @error('content')
                                    <span class="invalid-feedback">
                                        <small>{{ $message }}</small>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary border btn-block">@lang('Send')</button>
                            </div>

                        </form><br>
                    @endif
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-12 col-sm-12 col-lg-6 p-2">
            <div id="mapid" class="border shadow rounded info-left"></div>
        </div>
        <div class="col-12 col-sm-12 col-lg-6 p-2">
            <div class="text-paradox-2 border shadow rounded info-right" style="background-image:url('images/pages/geometrical-linear-pattern-260nw-621214532.png');">
                <div class="container"><br>
                    <a class="navbar-brand d-flex align-items-center">
                        <span class="pt-2 paradox">PARADOX WEB</span>
                    </a>
                    <hr style="margin-bottom:10px;">
                    <small>Nos preocupamos por tu negocio online y, por este motivo, encontraremos la mejor solución de hosting, desarrollaremos tu página web o aplicación online con las mejores garantías y, con nuestras técnicas de posicionamiento en los buscadores, te sorprenderás con los resultados.</small>
                    <br><hr style="margin-top:10px;"><br>
                    <div class="footer-pad">
                        <small>
                            <div class="row">
                                <div class="col">
                                    <address>
                                        <strong>PARADOX WEB</strong><br />
                                        <span class="oi oi-location"></span> Figueres, GI 17600<br />
                                        <abbr title="Phone"></abbr><span class="oi oi-phone"></span> +34 697321944<br>
                                        <span class="oi oi-envelope-closed"></span><a href="mailto:info@paradox-web.com"> info@paradox-web.com</a>
                                    </address>
                                </div>
                                <div class="col">
                                    <address>
                                        <strong>No disponemos de oficina fisica.</strong><br />
                                        <strong>Todos nuestros servicios son remotos.</strong><br />
                                        {{-- <a href="mailto:bertofern@gmail.com"><i class="fab fa-skype"></i> bertofern@gmail.com</a> --}}
                                        <i class="fab fa-skype fa-lg"></i>&nbsp;
                                        <i class="fas fa-video fa-lg"></i>
                                    </address>
                                </div>
                            </div>
                            <hr>
                            <a href="#">
                                <i class="fab fa-twitter fa-lg"></i>
                            </a>&nbsp;
                            <a href="#">
                                <i class="fab fa-facebook fa-lg"></i>
                            </a>&nbsp;
                            <a href="#">
                                <i class="fab fa-instagram fa-lg"></i>
                            </a>&nbsp;
                            <a href="#">
                                <i class="fab fa-linkedin fa-lg"></i>
                            </a>
                        </small>
                    </div><br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
