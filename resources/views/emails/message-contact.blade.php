<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Message Contact</title>
</head>
<body style="font-family: sans-serif;">
    <div style="display: block; margin: auto; max-width: 600px;" class="main">
        {{-- <img alt="Inspect with Tabs" src="https://assets-examples.mailtrap.io/integration-examples/welcome.png" style="width: 100%;"> --}}
        {{-- {{ var_dump($data) }} --}}
        <h1 style="font-size: 18px; font-weight: bold; margin-top: 20px">Reciviste un mensaje de: {{ $data['name'] }} - {{ $data['email'] }}</h1>
        <p><strong>Asunto:</strong> {{ $data['subject'] }}</p>
        <p><strong>Contenido:</strong> {{ $data['content'] }}</p>
    </div>
    <!-- Example of invalid for email html/css, will be detected by Mailtrap: -->
    <style>
        .main { background-color: white; }
        a:hover { border-left-width: 1em; min-height: 2em; }
    </style>
</body>
</html>
