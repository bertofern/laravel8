@extends('layouts.app-primary')

@section('title', trans('Courses'))

@section('content')
<div class="container">
    <div class="row align-items-center">
        <div class="col-6 font-weight-bold">@Lang('There are') {{ count($courses) }} @Lang('active courses')</div>
        <div class="col-6 text-right">
            {{-- <div class="btn-group-vertical">
                <span class="badge badge-info" style="width:100%;">New</span>
                <div class="btn-group" role="group" aria-label="Basic example">
                    <a class="btn btn-light btn-outline-secondary btn-sm" href="/courses/create">
                        <i class="fas fa-plus"></i>
                    </a>
                </div>
            </div>
            <div class="btn-group-vertical">
                <span class="badge badge-info" style="width:100%;">Grid</span>
                <div class="btn-group" role="group" aria-label="Basic example">
                    <a class="btn btn-light btn-outline-secondary btn-sm" id="grid1">
                        <i class="fas fa-grip-lines"></i>
                    </a>
                    <a class="btn btn-light btn-outline-secondary btn-sm" id="grid2">
                        <i class="fas fa-th-large"></i>
                    </a>
                    <a class="btn btn-dark text-white btn-sm" id="grid3">
                        <i class="fas fa-th"></i>
                    </a>
                </div>
            </div> --}}
        </div>
    </div><br>

    <?php $pos=1 ?>
    @forelse ($courses as $course)
        @if ($pos == 1)
            <div class="row">
        @endif
    {{-- <div class="grid-change col-4 float-left"> --}}
    <div class=" col-4 float-left">
        <div class="card">
            <div class="card-header">
                <a href="{{ route('courses.show', $course) }}">{{ $course->title }}</a>
            </div>
            <img class="card-img-top" src="images/default_10x1.png" alt="Card image cap">
            <div class="card-body">
                <p class="card-text">{{ $course->description }}</p>
                <p class="card-text collapse" id="collapseContent{{ $course->id }}">{{ $course->description }}</p>
                <a class="read-more text-decoration-none collapsed" data-toggle="collapse"
                    href="#collapseContent{{ $course->id }}" aria-controls="collapseContent{{ $course->id }}"
                    id="read-more{{ $course->id }}">
                    <small class="mark-color small-read-more" id="small{{ $course->id }}">Read more</small></a>
            </div>
            <div class="card-footer">
                <div class="media">
                    <img src="images/default_4x4.png" width="70" class="align-self-center mr-3 img-thumbnail" alt="Card image cap">
                    <div class="media-body">
                      <small>Albert Fernánez</small><br>
                      <small>Desarrolador Web</small><br>
                      <small>Desarrolador Web</small><br>
                    </div>
                </div>
                <div class="row">
                    <div class="col-6">
                        <p class="card-text text-left"><small class="text-muted">Created {{
                                $course->created_at->diffForHumans() }}</small></p>
                    </div>
                    <div class="col-6">
                        <p class="card-text text-right"><small class="text-muted">Last updated {{
                                $course->updated_at->diffForHumans() }}</small></p>
                    </div>
                </div>
            </div>
        </div><br>
    </div>
    @if ($pos %3 == 0)
        </div>
        <?php $pos=1 ?>
    @else
        <?php $pos++ ?>
    @endif

    @empty
    <p>No hay Cursos para mostrar</p>
    @endforelse

    {{ $courses->links() }}

</div>
@endsection
