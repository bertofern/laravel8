@extends('layouts.app-primary')

@section('title', trans('Course - Edit | ') . $course->title)

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-6">@Lang('Edit Course')</div>
                        <div class="col-6 text-right">
                            <div class="btn-group-vertical">
                                <span class="badge badge-info" style="width:100%;">Back</span>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a class="btn btn-light btn-outline-secondary btn-sm" href="/courses/create">
                                        <i class="fas fa-chevron-left"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    <form method="POST" action="{{ route('courses.update', $course) }}" >
                        <div class="form-row">
                            <div class="form-group col-md-6" style="border-right:2px solid grey;">
                                @csrf
                                @method('PATCH')
                                {{-- <input type="hidden" name="_method" value="PATCH"> --}}
                                <div class="form-group form-row mr-2">
                                    <label for="title" class="col-12">
                                        Title of course<br>
                                        <input name="title" class="form-control" placeholder="Title of course" value="{{ old('title', $course->title) }}">
                                    </label>
                                    @error('title')
                                        {{-- <div class="alert alert-danger">{{ $message }}</div> --}}
                                        <small>{{ $message }}</small><br>
                                    @enderror
                                </div>

                                <div class="form-group form-row mr-2">
                                    <label for="slug" class="col-12">
                                        URL of course<br>
                                        <input name="slug" class="form-control" placeholder="URL of course" value="{{ old('slug', $course->slug) }}">
                                    </label>
                                    @error('slug')
                                        <small>{{ $message }}</small><br>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group col-md-6">
                                <div class="form-group form-row ml-2">
                                    <label for="description" class="col-12">
                                        Description of course<br>
                                        <textarea name="description" class="form-control" rows="10">{{ old('description', $course->description) }}</textarea>
                                    </label>
                                    @error('description')
                                        <small>{{ $message }}</small><br>
                                    @enderror
                                </div>
                            </div>
                        </div>




                        <button class="btn btn-secondary btn-sm btn-block">@lang('Update')</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
