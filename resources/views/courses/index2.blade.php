@extends('layouts.app-primary')

@section('title', trans('Courses'))

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-6 font-weight-bold">@Lang('There are') {{ count($courses) }} @Lang('active courses')</div>
                        <div class="col-6 text-right">
                            <div class="btn-group-vertical">
                                <span class="badge badge-info" style="width:100%;">New</span>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a class="btn btn-light btn-outline-secondary btn-sm" href="/courses/create">
                                        <i class="fas fa-plus"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="btn-group-vertical">
                                <span class="badge badge-info" style="width:100%;">Grid</span>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a class="btn btn-dark text-white btn-sm" id="grid1">
                                        <i class="fas fa-grip-lines"></i>
                                    </a>
                                    <a class="btn btn-light btn-outline-secondary btn-sm" id="grid2">
                                        <i class="fas fa-th-large"></i>
                                    </a>
                                    <a class="btn btn-light btn-outline-secondary btn-sm" id="grid3">
                                        <i class="fas fa-th"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-body">

                        <ul class="list-group">
                            @forelse ($courses as $course)
                            <li class="list-group-item p-0">
                                <a class="d-flex align-items-center text-secondary" href="{{ route('courses.show', $course) }}">
                                    <span class="p-0"><img src="images/default_4x4.png" style="width: 60px;" alt="Card image cap"></span>
                                    <span class="text-secondary font-weight-bold p-2">{{ $course->title }}</span>
                                    <span class="text-black-50 ml-auto p-2">{{ $course->created_at->format('d/m/Y') }}</span>
                                </a>
                            </li>
                            @empty
                                <p>No hay Cursos para mostrar</p>
                            @endforelse
                        </ul>

                    {{ $courses->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
