@extends('layouts.app-primary')

@section('title', trans('Course | ') . $course->title)

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-6">{{ $course->title }}</div>
                        <div class="col-6 text-right">
                            <div class="btn-group-vertical">
                                <span class="badge badge-info" style="width:100%;">Back</span>
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a class="btn btn-light btn-outline-secondary btn-sm" href="/courses">
                                        <i class="fas fa-chevron-left"></i>
                                    </a>
                                </div>
                            </div>
                            @auth
                                <div class="btn-group-vertical">
                                    <span class="badge badge-info" style="width:100%;">Edit</span>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        {{-- <a class="btn btn-light btn-outline-secondary btn-sm" href="/courses/{{ $course->slug }}/edit"> --}}
                                        <a class="btn btn-light btn-outline-secondary btn-sm" href="{{ route('courses.edit', $course) }}">
                                            <i class="fas fa-pencil-alt"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="btn-group-vertical">
                                    <span class="badge badge-info" style="width:100%;">Delete</span>
                                    <div class="btn-group" role="group" aria-label="Basic example">
                                        <form method="POST" action="{{ route('courses.destroy', $course) }}" >
                                            @csrf
                                            @method('DELETE')
                                            <button class="btn btn-light btn-outline-secondary btn-sm">
                                                <i class="fas fa-trash"></i>
                                            </button>
                                        </form>

                                    </div>
                                </div>
                            @endauth
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    {{-- <img class="card-img-top" src="..." alt="Card image cap"> --}}
                    {{-- <h5 class="card-title">Card title</h5> --}}
                    <p class="card-text">{{ $course->description }}</p>
                    <p class="card-text"><small class="text-muted">Created {{ $course->created_at->diffForHumans() }}</small></p>
                    <p class="card-text"><small class="text-muted">Last updated {{ $course->updated_at->diffForHumans() }}</small></p>
                </div>
            </div>
            <br><div>@comments(['model' => $course])</div>
        </div>
    </div>
</div>
@endsection
