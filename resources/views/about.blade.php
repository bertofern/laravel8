@extends('layouts.app-primary')

@section('title', trans('Aboud'))

@section('breadcrumb')
<li class="breadcrumb-item" aria-current="page">
    <span class="d-inline-block icon-width oi oi-home"></span><a href="{{ url('/') }}">&nbsp;@lang('Home')</a>
</li>
<li class="breadcrumb-item active" aria-current="page"><a>@lang('About')</a></li>
@endsection

@section('content')

@include('layouts.proyects')
@include('layouts.filter-div')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
