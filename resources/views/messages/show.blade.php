@extends('layouts.messenger')

@section('title', trans('Messages | ' . $message->subject))

@section('content')

<div class="container pl-4">
    <nav class="navbar navbar-expand-lg navbar-light d-flex justify-content-between pl-0">
        <div>
            <a class="btn btn-light btn-outline-secondary btn-group" href="{{ route('messages.index') }}">
                <i class="fas fa-chevron-left"></i>
            </a>
            <h4 class="navbar-brand font-weight-bold btn-group">&nbsp&nbsp{{ $message->subject }}</h4>
        </div>

        <div>
            <span class="btn-group"><i class="fas fa-print"></i></span>
            <div class="btn-group" role="group" aria-label="Basic example">
                <form method="POST" action="{{ route('messages.destroy', $message) }}" >
                    @csrf
                    @method('DELETE')
                    <button class="btn ">
                        <i class="fas fa-trash"></i>
                    </button>
                </form>
            </div>
        </div>

    </nav>

    <div class="card shadow">
        <div class="card-header">
            <div class="media d-flex justify-content-between">
                @if ($from_user->profile_photo_path)
                    <img class="align-self-center mr-3" width="60"
                    src="{{asset('storage/' . $from_user->profile_photo_path)}}" alt="Profile Image">
                @else
                    <img class="align-self-center mr-3" width="60"
                        src="{{asset('images/default_4x4.png')}}" alt="Profile Image">
                @endif
                <div class="media-body">
                    <strong class="mt-0">{{ $from_user->name }}</strong><br>
                    <small class="mt-0">para {{ $to_user->name }}</small><br>
                </div>
                <small class="text-muted mr-4">{{ $message->created_at->diffForHumans() }}</small>
            </div>
        </div>
        <div class="card-body">
            <p class="card-text">{{ $message->message }}</p>
        </div>
    </div><br>

    <div class="card shadow">
        <div class="card-header bg-white">
            <div class="media d-flex justify-content-between">
                @if ($to_user->profile_photo_path)
                    <img class="align-self-center mr-3" width="60"
                    src="{{asset('storage/' . $to_user->profile_photo_path)}}" alt="Profile Image">
                @else
                    <img class="align-self-center mr-3" width="60"
                        src="{{asset('images/default_4x4.png')}}" alt="Profile Image">
                @endif
                <div class="media-body">
                    <small class="mt-0">para {{ $to_user->name }}</small><br>
                </div>
                <small class="text-muted mr-4">{{ $message->created_at->diffForHumans() }}</small>
            </div>
        </div>
        <form>
            <div class="card-body">
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
            </div>
            <div class="card-footer bg-white">
                <button class="btn btn-dark">Reply</button>
            </div>
        </form>
    </div>


</div>


@endsection
