@extends('layouts.messenger')

@section('title', trans('Messages'))

@section('content')

{{-- <div class="container">
    <div class="row align-items-center">
        <div class="col-6 font-weight-bold">@Lang('There are') {{ $totalMessages }} @Lang('messages - '){{
            $totalMessagesNoReaded }} @Lang(' no leidos')</div>
        <div class="col-6 text-right">
            <div class="btn-group-vertical">
                <span class="badge badge-info" style="width:100%;">New</span>
                <div class="btn-group" role="group" aria-label="Basic example">
                    <a class="btn btn-light btn-outline-secondary btn-sm" href="/messages/create">
                        <i class="fas fa-plus"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div> --}}


{{-- All messages --}}
<div class="table-responsive show" id="all-messages">
    <table class="table table-sm table-condensed">
        <thead class="thead-light">
            <tr>
                <th scope="col" width="1%"></th>
                <th scope="col" width="20%">User</th>
                <th scope="col" width="20%">Subject</th>
                <th scope="col">Message</th>
                <th scope="col" width="20%" class="text-right"></th>
            </tr>
        </thead>
        <tbody>
            @forelse ($allMessages as $message)
            {{-- <a href="{{ route('messages.show', $message) }}"></a> --}}
            <tr class="table-row" data-href="{{ route('messages.show', $message) }}">
                <td><input type="checkbox" id="msn{{ $message->id }}" name="msn{{ $message->id }}"
                        value="msn{{ $message->id }}"></td>
                <td @if(!$message->readed && $message->to_user_id == auth()->user()->id)class="font-weight-bold" @endif>{{ $message->from_user_name }}</td>
                <td class="truncate-content @if(!$message->readed && $message->to_user_id == auth()->user()->id) font-weight-bold @endif">
                    <div class="truncate-text">{{ $message->subject }}</div>
                </td>
                <td class="truncate-content @if(!$message->readed && $message->to_user_id == auth()->user()->id) font-weight-bold @endif">
                    <div class="truncate-text">{{ $message->message }}</div>
                </td>
                <td class="text-right @if(!$message->readed && $message->to_user_id == auth()->user()->id)font-weight-bold @endif">
                    @if($message->created_at->format('d/m/Y') == date("d/m/Y"))
                    {{ $message->created_at->format('h:i') }}&nbsp
                    @else
                    {{ $message->created_at->format('d M') }}&nbsp
                    @endif
                </td>
            </tr>
            @empty
            <p>No hay mensajes para mostrar</p>
            @endforelse
        </tbody>
    </table>
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
          {{-- {{ $allMessages->links() }} --}}
        </ul>
    </nav>
</div>


{{-- No-readed-messages --}}
<div class="table-responsive d-none" id="no-readed-messages">
    <table class="table table-sm table-condensed">
        <thead class="thead-light">
            <tr>
                <th scope="col" width="1%"></th>
                <th scope="col" width="20%">User</th>
                <th scope="col" width="20%">Subject</th>
                <th scope="col">Message</th>
                <th scope="col" width="20%" class="text-right"></th>
            </tr>
        </thead>
        <tbody>
            @forelse ($noReadedMessages as $message)
            {{-- <a href="{{ route('messages.show', $message) }}"></a> --}}
            <tr class="table-row" data-href="{{ route('messages.show', $message) }}">
                <td><input type="checkbox" id="msn{{ $message->id }}" name="msn{{ $message->id }}"
                        value="msn{{ $message->id }}"></td>
                <td @if(!$message->readed && $message->to_user_id == auth()->user()->id)class="font-weight-bold" @endif>{{ $message->from_user_name }}</td>
                <td class="truncate-content @if(!$message->readed && $message->to_user_id == auth()->user()->id) font-weight-bold @endif">
                    <div class="truncate-text">{{ $message->subject }}</div>
                </td>
                <td class="truncate-content @if(!$message->readed && $message->to_user_id == auth()->user()->id) font-weight-bold @endif">
                    <div class="truncate-text">{{ $message->message }}</div>
                </td>
                <td class="text-right @if(!$message->readed && $message->to_user_id == auth()->user()->id)font-weight-bold @endif">
                    @if($message->created_at->format('d/m/Y') == date("d/m/Y"))
                    {{ $message->created_at->format('h:i') }}&nbsp
                    @else
                    {{ $message->created_at->format('d M') }}&nbsp
                    @endif
                </td>
            </tr>
            @empty
            <p>No hay mensajes para mostrar</p>
            @endforelse
        </tbody>
    </table>
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
          {{ $noReadedMessages->links() }}
        </ul>
    </nav>
</div>

{{-- Sended-messages --}}
<div class="table-responsive d-none" id="sended-messages">
    <table class="table table-sm table-condensed">
        <thead class="thead-light">
            <tr>
                <th scope="col" width="1%"></th>
                <th scope="col" width="20%">User</th>
                <th scope="col" width="20%">Subject</th>
                <th scope="col">Message</th>
                <th scope="col" width="20%" class="text-right"></th>
            </tr>
        </thead>
        <tbody>
            @forelse ($sendedMessages as $message)
            {{-- <a href="{{ route('messages.show', $message) }}"></a> --}}
            <tr class="table-row" data-href="{{ route('messages.show', $message) }}">
                <td><input type="checkbox" id="msn{{ $message->id }}" name="msn{{ $message->id }}"
                        value="msn{{ $message->id }}"></td>
                <td @if(!$message->readed && $message->to_user_id == auth()->user()->id)class="font-weight-bold" @endif>{{ $message->from_user_name }}</td>
                <td class="truncate-content @if(!$message->readed && $message->to_user_id == auth()->user()->id) font-weight-bold @endif">
                    <div class="truncate-text">{{ $message->subject }}</div>
                </td>
                <td class="truncate-content @if(!$message->readed && $message->to_user_id == auth()->user()->id) font-weight-bold @endif">
                    <div class="truncate-text">{{ $message->message }}</div>
                </td>
                <td class="text-right @if(!$message->readed && $message->to_user_id == auth()->user()->id)font-weight-bold @endif">
                    @if($message->created_at->format('d/m/Y') == date("d/m/Y"))
                    {{ $message->created_at->format('h:i') }}&nbsp
                    @else
                    {{ $message->created_at->format('d M') }}&nbsp
                    @endif
                </td>
            </tr>
            @empty
            <p>No hay mensajes para mostrar</p>
            @endforelse
        </tbody>
    </table>
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
          {{ $sendedMessages->links() }}
        </ul>
    </nav>
</div>

{{-- Recived-messages --}}
<div class="table-responsive d-none" id="recived-messages">
    <table class="table table-sm table-condensed">
        <thead class="thead-light">
            <tr>
                <th scope="col" width="1%"></th>
                <th scope="col" width="20%">User</th>
                <th scope="col" width="20%">Subject</th>
                <th scope="col">Message</th>
                <th scope="col" width="20%" class="text-right"></th>
            </tr>
        </thead>
        <tbody>
            @forelse ($recivedMessages as $message)
            {{-- <a href="{{ route('messages.show', $message) }}"></a> --}}
            <tr class="table-row" data-href="{{ route('messages.show', $message) }}">
                <td><input type="checkbox" id="msn{{ $message->id }}" name="msn{{ $message->id }}"
                        value="msn{{ $message->id }}"></td>
                <td @if(!$message->readed && $message->to_user_id == auth()->user()->id)class="font-weight-bold" @endif>{{ $message->from_user_name }}</td>
                <td class="truncate-content @if(!$message->readed && $message->to_user_id == auth()->user()->id) font-weight-bold @endif">
                    <div class="truncate-text">{{ $message->subject }}</div>
                </td>
                <td class="truncate-content @if(!$message->readed && $message->to_user_id == auth()->user()->id) font-weight-bold @endif">
                    <div class="truncate-text">{{ $message->message }}</div>
                </td>
                <td class="text-right @if(!$message->readed && $message->to_user_id == auth()->user()->id)font-weight-bold @endif">
                    @if($message->created_at->format('d/m/Y') == date("d/m/Y"))
                    {{ $message->created_at->format('h:i') }}&nbsp
                    @else
                    {{ $message->created_at->format('d M') }}&nbsp
                    @endif
                </td>
            </tr>
            @empty
            <p>No hay mensajes para mostrar</p>
            @endforelse
        </tbody>
    </table>
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
          {{ $recivedMessages->links() }}
        </ul>
    </nav>
</div>

@endsection
