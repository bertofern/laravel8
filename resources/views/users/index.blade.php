@extends('layouts.app-primary')

@section('title', trans('Users'))

@section('content')

{{-- <div class="table-responsive"> --}}
<div class="container">
    <table id="table-users" class="table table-striped table-bordered table-sm table-condensed">
        <thead class="thead-light">
            <tr>
                <th scope="col" width="10%"></th>
                <th scope="col" width="20%">User</th>
                <th scope="col" width="20%">Email</th>
                <th scope="col" width="10%">Role</th>
                <th scope="col">Verified</th>
                <th scope="col">Status</th>
                <th scope="col">Last Connection</th>
                <th scope="col" width="1%">Created</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($users as $user)
            {{-- <a href="{{ route('users.show', $user) }}"></a> --}}
            <tr class="table-row" data-href="{{ route('users.show', $user) }}">
                {{-- <td><input type="checkbox" id="msn{{ $user->id }}" name="msn{{ $user->id }}" value="msn{{ $user->id }}"></td>} --}}
                <td class="text-center align-middle">
                    @if (!empty($user->profile_photo_path))
                        <img class="rounded mx-auto d-block" src="{{ asset('storage/' . $user->profile_photo_path)}}" style="width:50px;height:auto;" alt="Profile Image">
                    @else
                        <i class="fas fa-user fa-2x"></i>
                    @endif
                </td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>
                    @foreach ($user->roles as $role)
                        <span class="badge rounded-pill bg-secondary">{{ $role->name }}</span>
                    @endforeach
                </td>
                <td>{{ $user->email_verified_at }}</td>
                <td>
                    @if(Cache::has('user-is-online-' . $user->id))
                        <span class="badge bg-success">Online</span>
                    @else
                        <span class="badge bg-danger">Offline</span>
                    @endif
                </td>
                <td>
                    @if($user->last_conection != null)
                        {{-- {{ $user->last_conection }} --}}
                        {{ \Carbon\Carbon::parse($user->last_conection)->diffForHumans() }}
                    @else
                        <span class="">Never</span>
                    @endif
                </td>
                {{-- <td>
                    @if($user->created_at->format('d/m/Y') == date("d/m/Y"))
                    {{ $user->created_at->format('h:i') }}&nbsp
                    @else
                    {{ $user->created_at->format('d M') }}&nbsp
                    @endif
                </td> --}}
                <td>
                    <div class="btn-group-vertical">
                        <div class="btn-group" role="group" aria-label="Basic example">
                            <a class="btn btn-primary btn-sm" href="{{ route('users.edit', $user) }}">
                                <i class="fas fa-pencil-alt"></i>
                            </a>
                            <a class="btn btn-danger btn-sm" href="#" onclick="document.getElementById('delete-user-form')">
                                <i class="fas fa-trash"></i>
                            </a>
                        </div>
                    </div>

                    <form id="delete-user-form" method="POST" action="{{ route('users.destroy', $user) }}" >
                        @csrf
                        @method('DELETE')
                    </form>

                </td>
            </tr>
            @empty
            <p>No hay usuarios para mostrar</p>
            @endforelse
        </tbody>
    </table>
    {{-- <div class="pagination justify-content-center">
        {{ $users->links() }}
    </div> --}}
    {{-- <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
          {{ $users->links() }}
        </ul>
    </nav> --}}
</div>

@endsection
