@extends('layouts.app-primary')

@section('title', trans('Users | ') . $user->name)

@section('css-after')
<style>
    .btn-users {
        color: white;
    }
</style>
@endsection

@section('content')
<div class="container">
    <nav class="navbar navbar-expand-lg d-flex justify-content-between pl-0">
        <div class="btn-group ml-2">
            <a class="btn btn-outline-light" href="{{ route('users.index') }}">
                <i class="fas fa-chevron-left"></i>
            </a>
        </div>
        <div class="text-center">
            <p class="">{{ $user->name }}</p>
        </div>
        <div>
            <button class="btn btn-group btn-sm btn-outline-dark mr-2">
                <i class="fas fa-pen"></i>
            </button>
            <button class="btn btn-group btn-sm btn-outline-light mr-2">
                <i class="fas fa-file-alt"></i>
            </button>
            <div class="btn-group" role="group">
                <button class="btn btn-sm btn-outline-light mr-2">
                    <i class="fas fa-file-alt"></i>
                </button>
            </div>
            <div class="btn-group" role="group">
                <form method="POST" action="{{ route('users.destroy', $user) }}" >
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-sm btn-outline-light mr-2">
                        <i class="fas fa-trash"></i>
                    </button>
                </form>
            </div>
            {{-- <button class="btn-group mr-2 btn-users">
                <i class="fas fa-file-alt"></i>
            </button>
            <div class="btn-group mr-2" role="group">
                <form method="POST" action="{{ route('users.destroy', $user) }}" >
                    @csrf
                    @method('DELETE')
                    <button class="btn-users">
                        <i class="fas fa-trash"></i>
                    </button>
                </form>
            </div> --}}
        </div>
    </nav><br>
    <div class="row">
        <div class="col-3">
            <div class="card shadow">
                <div style="position: relative; ">
                    <div style="position:absolute;background-color:rgb(31, 46, 95);width:100%;height:50px">&nbsp;</div>
                    <div style="position:absolute;top:30px;left:0;right:0;margin: auto;">
                        <div class="text-center">
                            @if (!empty($user->profile_photo_path))
                                <img class="rounded mx-auto d-block" src="{{ asset('storage/' . $user->profile_photo_path)}}" style="width:100px;height:100px;" alt="Profile Image">
                            @else
                                <i class="fas fa-user fa-4x" style="width:100px;height:100px;"></i>
                            @endif
                            <p class="card-text">{{ $user->name }}</p>
                            <p class="card-text">{{ $user->email }}</p>
                            <small class="text-muted">Programmer - Web Developer (Junior), in Interactiva Online.</small>
                        </div>
                    </div>
                </div>
                {{-- <div class="card-header bg-white text-center">
                    <div style="position: relative;">
                        <div style="position:absolute;background-color:rgb(220, 124, 21, 0.5);width:100%;height:50px">&nbsp;</div>
                        <div style="position:absolute;top:30px;left:0;right:0;margin: auto;">
                            <img class="rounded mx-auto d-block" src="{{asset('storage/' . $user->profile_photo_path)}}" style="width:100px;height:100px;" alt="Profile Image">
                        </div>
                    </div><br><br>
                    <div class="" style="background-color:rgb(220, 124, 21, 0.5);width:100%;height:80px">&nbsp;</div>
                    <img class="rounded mx-auto d-block" src="{{asset('storage/' . $user->profile_photo_path)}}" style="width:100px;height:100px;" alt="Profile Image">
                    <p class="card-text">{{ $user->name }}</p>
                    <p class="card-text">{{ $user->email }}</p>
                    <small class="text-muted">Programmer - Web Developer (Junior), in Interactiva Online.</small>
                </div> --}}
                <div class="card-body" style="margin-top: 250px;">

                    <p class="card-text"><small class="text-muted">Created {{ $user->created_at->diffForHumans() }}</small></p>
                </div>
                <div class="card-footer bg-white">
                    <p class="card-text"><small class="text-muted"><i class="far fa-calendar-alt"></i>&nbsp&nbsp Created {{ $user->created_at->diffForHumans() }}</small></p>
                </div>
                <div class="card-footer text-center bg-white">
                    <a class="btn badge text-white" style="background-color:#dc7c15"><i class="fas fa-paper-plane"></i>&nbsp&nbsp Send Message</a>
                </div>
            </div>
        </div>
        <div class="col-9">
            <div class="card shadow">
                <div class="card-body">
                    <img class="mx-auto d-block" src="{{asset('images/default_10x1.png')}}" alt="Card image cap">
                    <br><hr><br>
                    <p class="card-text">Inglés - Intermediate II </p>
                    <p class="card-text">Estatus: Inscrito</p>
                    <p class="card-text">Descripcion: Id expedita hic officia rem recusandae modi. Quis omnis vel quisquam dolorem qui aut ea suscipit. Ut atque doloremque ducimus. Iusto quasi optio nulla voluptatibus velit sapiente.</p>
                    <p class="card-text"><small class="text-muted">Created {{ $user->created_at->diffForHumans() }}</small></p>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
