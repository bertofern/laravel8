@extends('layouts.app-primary')

@section('content')

{{-- FIRTS START RATING --}}
<style>
#form { width: 250px;  margin: 0 auto; height: 50px; }
#form p { text-align: center; }
#form label { font-size: 20px; }
input[type="radio"] { display: none; }
label { color: grey; }
.clasificacion { direction: rtl; unicode-bidi: bidi-override; }
label:hover, label:hover ~ label { color: orange; }
input[type="radio"]:checked ~ label { color: orange; }
</style>
<form>
    <p class="clasificacion">
      <input id="radio1" type="radio" name="estrellas" value="5"><!--
      --><label for="radio1">★</label><!--
      --><input id="radio2" type="radio" name="estrellas" value="4"><!--
      --><label for="radio2">★</label><!--
      --><input id="radio3" type="radio" name="estrellas" value="3"><!--
      --><label for="radio3">★</label><!--
      --><input id="radio4" type="radio" name="estrellas" value="2"><!--
      --><label for="radio4">★</label><!--
      --><input id="radio5" type="radio" name="estrellas" value="1"><!--
      --><label for="radio5">★</label>
    </p>
</form>
{{-- END --}}



  {{-- Vista de la valoracion  https://codepen.io/FredGenkin/pen/eaXYGV --}}
  <div class="stars" style="--rating: 2.3;">
<style>
/* resouces/scss/star-rating.scss */
</style>
{{-- END --}}


{{-- Start rating form  https://codepen.io/knyttneve/pen/EBNqPN --}}
<div class="rating">
    <input type="radio" name="rating" id="rating-5">
    <label for="rating-5"></label>
    <input type="radio" name="rating" id="rating-4">
    <label for="rating-4"></label>
    <input type="radio" name="rating" id="rating-3">
    <label for="rating-3"></label>
    <input type="radio" name="rating" id="rating-2">
    <label for="rating-2"></label>
    <input type="radio" name="rating" id="rating-1">
    <label for="rating-1"></label>
  </div>
  <style>
.rating { display: flex; width: 100%; justify-content: center; overflow: hidden;
  flex-direction: row-reverse; height: 150px; position: relative; }
.rating-0 { filter: grayscale(100%); }
.rating > input { display: none; }
.rating > label { cursor: pointer; width: 40px; height: 40px; margin-top: auto;
  background-image: url("data:image/svg+xml;charset=UTF-8,%3csvg xmlns='http://www.w3.org/2000/svg' width='126.729' height='126.73'%3e%3cpath fill='%23e3e3e3' d='M121.215 44.212l-34.899-3.3c-2.2-.2-4.101-1.6-5-3.7l-12.5-30.3c-2-5-9.101-5-11.101 0l-12.4 30.3c-.8 2.1-2.8 3.5-5 3.7l-34.9 3.3c-5.2.5-7.3 7-3.4 10.5l26.3 23.1c1.7 1.5 2.4 3.7 1.9 5.9l-7.9 32.399c-1.2 5.101 4.3 9.3 8.9 6.601l29.1-17.101c1.9-1.1 4.2-1.1 6.1 0l29.101 17.101c4.6 2.699 10.1-1.4 8.899-6.601l-7.8-32.399c-.5-2.2.2-4.4 1.9-5.9l26.3-23.1c3.8-3.5 1.6-10-3.6-10.5z'/%3e%3c/svg%3e");
  background-repeat: no-repeat; background-position: center; background-size: 76%; transition: .3s; }
.rating > input:checked ~ label, .rating > input:checked ~ label ~ label {
  background-image: url("data:image/svg+xml;charset=UTF-8,%3csvg xmlns='http://www.w3.org/2000/svg' width='126.729' height='126.73'%3e%3cpath fill='%23fcd93a' d='M121.215 44.212l-34.899-3.3c-2.2-.2-4.101-1.6-5-3.7l-12.5-30.3c-2-5-9.101-5-11.101 0l-12.4 30.3c-.8 2.1-2.8 3.5-5 3.7l-34.9 3.3c-5.2.5-7.3 7-3.4 10.5l26.3 23.1c1.7 1.5 2.4 3.7 1.9 5.9l-7.9 32.399c-1.2 5.101 4.3 9.3 8.9 6.601l29.1-17.101c1.9-1.1 4.2-1.1 6.1 0l29.101 17.101c4.6 2.699 10.1-1.4 8.899-6.601l-7.8-32.399c-.5-2.2.2-4.4 1.9-5.9l26.3-23.1c3.8-3.5 1.6-10-3.6-10.5z'/%3e%3c/svg%3e");
}
.rating > input:not(:checked) ~ label:hover, .rating > input:not(:checked) ~ label:hover ~ label {
  background-image: url("data:image/svg+xml;charset=UTF-8,%3csvg xmlns='http://www.w3.org/2000/svg' width='126.729' height='126.73'%3e%3cpath fill='%23d8b11e' d='M121.215 44.212l-34.899-3.3c-2.2-.2-4.101-1.6-5-3.7l-12.5-30.3c-2-5-9.101-5-11.101 0l-12.4 30.3c-.8 2.1-2.8 3.5-5 3.7l-34.9 3.3c-5.2.5-7.3 7-3.4 10.5l26.3 23.1c1.7 1.5 2.4 3.7 1.9 5.9l-7.9 32.399c-1.2 5.101 4.3 9.3 8.9 6.601l29.1-17.101c1.9-1.1 4.2-1.1 6.1 0l29.101 17.101c4.6 2.699 10.1-1.4 8.899-6.601l-7.8-32.399c-.5-2.2.2-4.4 1.9-5.9l26.3-23.1c3.8-3.5 1.6-10-3.6-10.5z'/%3e%3c/svg%3e");
}
  </style>
  {{-- END --}}



  {{-- Start rating form   https://codepen.io/fusco/pen/MwawEL --}}
    <div class="rating">
        <input type="radio" name="rating" id="r1">
        <label for="r1"></label>
        <input type="radio" name="rating" id="r2">
        <label for="r2"></label>
        <input type="radio" name="rating" id="r3">
        <label for="r3"></label>
        <input type="radio" name="rating" id="r4">
        <label for="r4"></label>
        <input type="radio" name="rating" id="r5">
        <label for="r5"></label>
    </div>
    <style>
@mixin star-rating( $filled-color: #F9BF3B, $empty-color: #444, $size: 80px, $width: 400px) {
 $star--filled: ★;
 $star--empty: ☆;
 width: $width;
 > * {
  float: right;
 }
 // optional initial pulse of stars
 @at-root {
  @keyframes pulse {
   50% {
    color: lighten($empty-color, 10%);
    text-shadow: 0 0 15px lighten($empty-color, 20%);
   }
  }
 }
 label {
  height: 40px;
  width: 20%;
  display: block;
  position: relative;
  cursor: pointer;
  @for $i from 5 through 1 {
   &:nth-of-type(#{$i}):after {
    $animation-delay: $i * .05s;
    animation-delay: $animation-delay;
   }
  }
  &:after {
   transition: all 0.4s ease-out;
   -webkit-font-smoothing: antialiased;
   position: absolute;
   content: '#{$star--empty}';
   color: $empty-color;
   top: 0;
   left: 0;
   width: 100%;
   height: 100%;
   text-align: center;
   font-size: $size;
   animation: 1s pulse ease;
  }
  &:hover:after {
   color: lighten($empty-color, 10%);
   text-shadow: 0 0 15px lighten($empty-color, 10%);
  }
 }
 input {
  display: none;
  &:checked {
   + label:after,
   ~ label:after {
    content: '#{$star--filled}';
    color: $filled-color;
    text-shadow: 0 0 20px $filled-color;
   }
  }
 }
}

.rating {
 margin: 50px auto;
 @include star-rating();
}

body {
 background-color: #fff;
}
    </style>
    {{-- END --}}


  @endsection
