const { includes } = require('lodash');

require('./bootstrap');
require('alpinejs');

// READ MORE FUNCTIONS
$('.read-more').click(function(){
    var regex = /(\d+)/g;
    var numberId = this.id.match(regex);
    // var textId = $("#text-read-more" + numberId).text();

    var idd = $("#" + this.id).children().attr("id");
    if($("#" + idd).text().indexOf('Read more') >= 0) {
        // $("#collapseContent" + numberId).text(textId);
        $("#" + idd).text('Read less');
    }
    else if($("#" + idd).text().indexOf('Read less') >= 0) {
        // $("#collapseContent" + numberId).text(textId.slice(0, 10)+'...');
        $("#" + idd).text('Read more');
    }
});

// GRID FUNCTIONS
$('#grid1').click(function(){
    $( ".grid-change" ).removeClass( "col-6 col-4" ).addClass( "col-12" );
    $( "#grid1" ).removeClass( "btn-light btn-outline-secondary" ).addClass( "btn-dark text-white" );
    $( "#grid2" ).removeClass( "btn-dark text-white" ).addClass( "btn-light btn-outline-secondary" );
    $( "#grid3" ).removeClass( "btn-dark text-white" ).addClass( "btn-light btn-outline-secondary" );
});
$('#grid2').click(function(){
    $( ".grid-change" ).removeClass( "col-12 col-4" ).addClass( "col-6" );
    $( "#grid2" ).removeClass( "btn-light btn-outline-secondary" ).addClass( "btn-dark text-white" );
    $( "#grid1" ).removeClass( "btn-dark text-white" ).addClass( "btn-light btn-outline-secondary" );
    $( "#grid3" ).removeClass( "btn-dark text-white" ).addClass( "btn-light btn-outline-secondary" );
});
$('#grid3').click(function(){
    $( ".grid-change" ).removeClass( "col-6 col-12" ).addClass( "col-4" );
    $( "#grid3" ).removeClass( "btn-light btn-outline-secondary" ).addClass( "btn-dark text-white" );
    $( "#grid2" ).removeClass( "btn-dark text-white" ).addClass( "btn-light btn-outline-secondary" );
    $( "#grid1" ).removeClass( "btn-dark text-white" ).addClass( "btn-light btn-outline-secondary" );
});

// Messages row cliclable
$(".table-row").click(function() { window.document.location = $(this).data("href"); });


