About me: [bertofern](https://bertofern.wordpress.com/)

### Udemy - Dominando Laravel de principiante a experto

## Instación y Uso

**Instalar paquetes de Node**

```
$ npm install
```

**Compilacion de archivos MIX**

```
$ npm run dev // ejecuta el depurador y ejecuta el mix
$ npm run watch // ejecuta el dev en tiempo real
```

**Configurar variables de Entorno en '.env'**

*Copiar el archivo .envExample a .env y añadir las variables a este archivo.*

*Aseguarse de que .gitignore ignora .env para proteger las variables.*

*Comando para generar una nueva key del proyecto*

```
$ php artisan key:generate
```

*Se puede cambiar el nombre de la aplicación en 'APP_URL'*

*Comandos para limpiar la cache en caso de cambiar variables*

```
$ php artisan config:clear
$ php artisan cache:clear
$ composer dump-autoload
```

*Comandos en el caso de cambiar rutas*

```
$ php artisan route:list
$ php artisan route:list --name=courses
$ php artisan route:cache
```

*Crear una base de datos y configurar las siguientes variables*

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel_app_jetstream
DB_USERNAME=root
DB_PASSWORD=
```

*Migrar la base de datos y añadir datos de prueba*

```
$ php artisan migrate
$ composer dump-autoload   // Si da problemas al hacer seed
$ php artisan db:seed
```

*El principal usuario para hacer las pruebas es:*

```
user mail: user1@example.com
user password: password
```

**Servicios de Mail**

*Hay preconfigurados varios servicios de mail en .env. Descomentar y configurar el que se quiera utilizar.*

*Por defecto se usa un servicio de testeo de mail gratuito. https://ethereal.email/*

*ETHEREAL_USER_DEV=MAIL_USERNAME y ETHEREAL_PASS_DEV=MAIL_PASSWORD son las variables para este servicio de testeo de mail.*


**Para arrancar el servidor (a no ser que se use Laragon)**

```
$ php artisan serve
```

**Si se usa Laragon bastará con reiniciarlo y introducir en el navegador el 'nombre_del_proyecto + .test'**

```
http://laravel-app-jetstream.test/
```


---

**Configuración de Jetstream. config/fortify.php**

*Comentar o descomentar las siguientes lineas para activar o desactivar los siguientes modulos.*

```
Features::registration(),
Features::resetPasswords(),
Features::emailVerification(),
Features::updateProfileInformation(),
Features::updatePasswords(),
Features::twoFactorAuthentication([
    'confirmPassword' => true,
]),
```

---

**Storage**

*Para que estos archivos sean accesibles desde la web, se debe crear un enlace simbólico desde public/storage hasta storage/app/public.*

```
$ php artisan storage:link
```
