<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\MessageController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

// Route::view('/', 'home')->name('home');
Route::view('/home', 'home')->name('home')->middleware('translate');

// Route::get('/', function () { return view('welcome'); });
Route::view('/', 'welcome')->name('welcome')->middleware('translate');

Route::view('/about', 'about')->name('about')->middleware('translate');
Route::view('/portfolio', 'portfolio')->name('portfolio')->middleware('translate');

Route::view('/contact', 'contact')->name('contact')->middleware('translate');
Route::post('/contact', [ContactController::class, 'store'])->name('contact.store');

Route::view('/sidebar', 'sidebar')->name('sidebar')->middleware('translate');

// Route::get('/courses', [CourseController::class, 'index'])->name('courses.index');
// Route::get('/courses/create', [CourseController::class, 'create'])->name('courses.create');
// Route::post('/courses', [CourseController::class, 'store'])->name('courses.store');
// Route::get('/courses/edit/{course}', [CourseController::class, 'edit'])->name('courses.edit');
// Route::patch('/courses/update/{course}', [CourseController::class, 'update'])->name('courses.update');
// Route::delete('/courses/{course}', [CourseController::class, 'destroy'])->name('courses.destroy');
// // Route::get('/courses/{id}', [CourseController::class, 'show'])->name('courses.show');
// // Rutas Amigables -> poner al final
// Route::get('/courses/{course}', [CourseController::class, 'show'])->name('courses.show');

Route::resource('courses', CourseController::class)->middleware('translate');
// Route::resource('projects', CourseController::class)->names('courses')->parameters(['projects' => 'course']);

Route::resource('messages', MessageController::class)->middleware('auth')->middleware('translate');
Route::resource('users', UserController::class)->middleware('auth')->middleware('translate');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');

// Evitar que el registro esté activo
// Auth::routes(['registrer' => false]);


//LANG
Route::get('/lang/{language}', function ($language) {
    Session::put('language',$language);
    return redirect()->back();
})->name('language');
