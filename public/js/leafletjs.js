var map = L.map('mapid', {
    center: [42.26566687627625, 2.9580919912840358],
    zoom: 15
});

map.touchZoom.disable();
map.doubleClickZoom.disable();
map.scrollWheelZoom.disable();
map.boxZoom.disable();
map.keyboard.disable();


// *********** MAPAS *********** //
// L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
//     attribution: '&copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
// }).addTo(map)

L.tileLayer('https://tiles.stadiamaps.com/tiles/alidade_smooth_dark/{z}/{x}/{y}{r}.png', {
    attribution: '&copy; <a href="https://stadiamaps.com/">Stadia Maps</a>, &copy; <a href="https://openmaptiles.org/">OpenMapTiles</a> &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors',
}).addTo(map)

// *********** ICONS *********** //
var orangeIcon = new L.Icon({
    iconUrl: 'https://raw.githubusercontent.com/pointhi/leaflet-color-markers/master/img/marker-icon-2x-yellow.png',
    shadowUrl: 'https://cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.7/images/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });



var marcador = L.marker([42.26566687627625, 2.9580919912840358], {
    icon: orangeIcon,
    title: 'PARADOX WEB',     // Add a title
    color: 'red',
    opacity: 1}).addTo(map);
marcador.bindPopup(`<b>PARADOX WEB</b><br><b>Web: <a href="#">https://paradox-web.com</a></b><br>Mail: info@paradox-web.com<br>`).openPopup();

