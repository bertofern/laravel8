// Efects scroll
ScrollReveal({ reset: true });
ScrollReveal().reveal('.headline', { delay: 500 });
window.sr = ScrollReveal();
sr.reveal('.info-right', {
    duration: 2000,
    origin:'bottom',
    distance:'300px',
    viewFactor: 0.2
});
sr.reveal('.info-left', {
    duration: 3000,
    origin:'bottom',
    distance:'300px',
    viewFactor: 0.2
});



//Get the button
var mybutton = document.getElementById("btnToTop");

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
    } else {
    mybutton.style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
