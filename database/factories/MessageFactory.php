<?php

namespace Database\Factories;

use App\Models\Message;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;

class MessageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Message::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition() {
        $conversation_id= Message::max('conversation_id');
        $dateRandom = $this->faker->dateTimeBetween('+0 days', '+2 days');
        $from_user_id = User::all()->random()->id;
        $to_user_id = User::all()->random()->id;


        // $conversation_id=3;
        // $from_user_id = 1;
        // $to_user_id = 6;
        // $dateRandom = $this->faker->dateTimeBetween('+5 days', '+5 days');

        return [
            'conversation_id' => $conversation_id + 1,
            'from_user_id' => $from_user_id,
            'from_user_name' => User::where('id', $from_user_id)->value('name'),
            'to_user_id' => $to_user_id,
            'to_user_name' => User::where('id', $to_user_id)->value('name'),
            'subject' => $this->faker->sentence,
            'message' => $this->faker->paragraph,
            'readed' => $this->faker->boolean(),
            'created_at' => $dateRandom,
            'updated_at' => $dateRandom,


            // 'conversation_id' => $conversation_id + 1,
            // 'from_user_id' => $from_user_id,
            // 'from_user_name' => User::where('id', $from_user_id)->value('name'),
            // 'to_user_id' => $to_user_id,
            // 'to_user_name' => User::where('id', $to_user_id)->value('name'),
            // 'subject' => $this->faker->sentence,
            // 'message' => $this->faker->paragraph,
            // 'readed' => 0,
            // 'created_at' => $dateRandom,
            // 'updated_at' => $dateRandom,

        ];
    }
}
