<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->id();
            $table->integer('conversation_id')->nullable();
            $table->foreignId('from_user_id')->constrained('users')->onUpdate('cascade')->onDelete('cascade');
            $table->string('from_user_name');
            $table->foreignId('to_user_id')->constrained('users')->onUpdate('cascade')->onDelete('cascade');
            $table->string('to_user_name');
            $table->string('subject');
            $table->text('message');
            $table->boolean('readed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
