<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
// use Database\Seeders\MessageSeeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {

        $this->call(RoleSeeder::class);

        $this->call(UserSeeder::class);
        \App\Models\User::factory(10)->create();

        \App\Models\Course::factory(10)->create();

        $this->call(MessageSeeder::class);
        \App\Models\Message::factory(1)->create();
        \App\Models\Message::factory(1)->create();

        $this->call(TeamSeeder::class);
    }
}
