<?php

namespace Database\Seeders;

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use App\Models\Team;
use App\Models\User;
use Illuminate\Support\Str;

class TeamSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        // Example
        // $team = new Team();
        // $team->user_id = 1;
        // $team->name = 'Company';
        // $team->personal_team = false;
        // $team->save();

        $faker = Faker::create();

        $team = new Team();
        $team->user_id = 1;
        $team->name = 'Company';
        $team->personal_team = false;
        $team->save();

    }

}
