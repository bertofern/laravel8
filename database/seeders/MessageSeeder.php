<?php

namespace Database\Seeders;

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use App\Models\Message;
use App\Models\User;

class MessageSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        // $message = new Message();
        // $from_user_id = User::all()->random()->id;
        // $message->from_user_id = $from_user_id;
        // $message->from_user_name = User::where('id', $from_user_id)->value('name');
        // $to_user_id = User::all()->random()->id;
        // $message->to_user_id = $to_user_id;
        // $message->to_user_name = User::where('id', $to_user_id)->value('name');
        // $message->subject = $faker->sentence;
        // $message->message = $faker->paragraph;
        // $message->readed = $faker->boolean();
        // $dateRandom = $faker->dateTimeBetween('+0 days', '+2 days');
        // $message->created_at = $dateRandom;  // 2020-12-21 17:52:47
        // $message->updated_at = $dateRandom;
        // $message->save();

        $faker = Faker::create();

        $message = new Message();
        $message->conversation_id=1;
        $from_user_id = 1;
        $message->from_user_id = $from_user_id;
        $message->from_user_name = User::where('id', $from_user_id)->value('name');
        $to_user_id = 6;
        $message->to_user_id = $to_user_id;
        $message->to_user_name = User::where('id', $to_user_id)->value('name');
        $message->subject = $faker->sentence;
        $message->message = $faker->paragraph;
        $message->readed = 0;
        $dateRandom = $faker->dateTimeBetween('+6 days', '+6 days');
        $message->created_at = $dateRandom;
        $message->updated_at = $dateRandom;
        $message->save();

        $message = new Message();
        $message->conversation_id=1;
        $from_user_id = 6;
        $message->from_user_id = $from_user_id;
        $message->from_user_name = User::where('id', $from_user_id)->value('name');
        $to_user_id = 1;
        $message->to_user_id = $to_user_id;
        $message->to_user_name = User::where('id', $to_user_id)->value('name');
        $message->subject = $faker->sentence;
        $message->message = $faker->paragraph;
        $message->readed = 0;
        $dateRandom = $faker->dateTimeBetween('+5 days', '+5 days');
        $message->created_at = $dateRandom;
        $message->updated_at = $dateRandom;
        $message->save();

        $message = new Message();
        $message->conversation_id=1;
        $from_user_id = 1;
        $message->from_user_id = $from_user_id;
        $message->from_user_name = User::where('id', $from_user_id)->value('name');
        $to_user_id = 6;
        $message->to_user_id = $to_user_id;
        $message->to_user_name = User::where('id', $to_user_id)->value('name');
        $message->subject = $faker->sentence;
        $message->message = $faker->paragraph;
        $message->readed = 0;
        $dateRandom = $faker->dateTimeBetween('+6 days', '+6 days');
        $message->created_at = $dateRandom;
        $message->updated_at = $dateRandom;
        $message->save();

        $message = new Message();
        $message->conversation_id=1;
        $from_user_id = 6;
        $message->from_user_id = $from_user_id;
        $message->from_user_name = User::where('id', $from_user_id)->value('name');
        $to_user_id = 1;
        $message->to_user_id = $to_user_id;
        $message->to_user_name = User::where('id', $to_user_id)->value('name');
        $message->subject = $faker->sentence;
        $message->message = $faker->paragraph;
        $message->readed = 0;
        $dateRandom = $faker->dateTimeBetween('+5 days', '+5 days');
        $message->created_at = $dateRandom;
        $message->updated_at = $dateRandom;
        $message->save();
    }

}
