<?php

namespace Database\Seeders;

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use App\Models\Role;
use Illuminate\Support\Str;

class RoleSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $faker = Faker::create();

        $role = new Role();
        $role->name = 'super-admin';
        $role->display_name = 'Super Administrador';
        $role->description = 'Tiene acceso total';
        $role->save();

        $role = new Role();
        $role->name = 'admin';
        $role->display_name = 'Administrador';
        $role->description = 'Tiene acceso a la parte administrativa';
        $role->save();

        $role = new Role();
        $role->name = 'editor';
        $role->display_name = 'Editor';
        $role->description = 'Tiene acceso para editar';
        $role->save();

        $role = new Role();
        $role->name = 'user';
        $role->display_name = 'Usuario';
        $role->description = 'Tiene acceso de usuario';
        $role->save();

    }

}
