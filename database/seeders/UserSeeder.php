<?php

namespace Database\Seeders;

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Team;
use App\Models\Role;
use App\Models\RoleUser;
use Illuminate\Support\Str;

class UserSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $faker = Faker::create();

        // $user = new User();
        // $user->name = 'Nora Mills';
        // $user->email = 'user1@example.com';
        // $user->email_verified_at = now();
        // $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi'; // password
        // $user->remember_token = Str::random(10);
        // $user->profile_photo_path = 'photos/lvc2FOvCJp5pvKDDQBPa1yLIDTsowKMOMBsXUX4q.png';
        // $role = Role::where('name', 'super-admin')->first();
        // $user->role_id = $role->id;
        // $user->save();
        // $team = new Team();
        // $team->user_id = $user->id;
        // $team->name = $user->name;
        // $team->personal_team = 1;
        // $team->save();

        $user = new User();
        $user->name = 'Nora Mills';
        $user->email = 'user1@example.com';
        $user->email_verified_at = now();
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi'; // password
        $user->remember_token = Str::random(10);
        $user->profile_photo_path = 'photos/lvc2FOvCJp5pvKDDQBPa1yLIDTsowKMOMBsXUX4q.png';
        $user->save();
        $team = new Team();
        $team->user_id = $user->id;
        $team->name = $user->name;
        $team->personal_team = 1;
        $team->save();
        $role_user = new RoleUser();
        $role = Role::where('name', 'super-admin')->first();
        $role_user->role_id = $role->id;
        // $user = User::where('name', 'Nora Mills')->first();
        $role_user->user_id = $user->id;
        $role_user->save();

        $user = new User();
        $user->name = 'Elvera Wiza';
        $user->email = 'user2@example.com';
        $user->email_verified_at = now();
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi'; // password
        $user->remember_token = Str::random(10);
        $user->profile_photo_path = 'photos/gbuZqsDfeETyAx03UkTLCGpGzdgbaA7iW68rmzaC.png';
        $user->save();
        $team = new Team();
        $team->user_id = $user->id;
        $team->name = $user->name;
        $team->personal_team = 1;
        $team->save();
        $role_user = new RoleUser();
        $role = Role::where('name', 'admin')->first();
        $role_user->role_id = $role->id;
        $role_user->user_id = $user->id;
        $role_user->save();

        $user = new User();
        $user->name = 'Brady Haley';
        $user->email = 'user3@example.com';
        $user->email_verified_at = now();
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi'; // password
        $user->remember_token = Str::random(10);
        $user->profile_photo_path = 'photos/BewWE8c8a2g1CwQLdgGdlgBE16yaqlUrcjHyjq8T.png';
        $user->save();
        $team = new Team();
        $team->user_id = $user->id;
        $team->name = $user->name;
        $team->personal_team = 1;
        $team->save();
        $role_user = new RoleUser();
        $role = Role::where('name', 'editor')->first();
        $role_user->role_id = $role->id;
        $role_user->user_id = $user->id;
        $role_user->save();

        $user = new User();
        $user->name = "Maria O'Keefe";
        $user->email = 'user4@example.com';
        $user->email_verified_at = now();
        $user->password = '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi'; // password
        $user->remember_token = Str::random(10);
        $user->profile_photo_path = 'photos/aWttFRzXTCpuAAGSK0CGftyGF38nEtwGk2PPY5NK.png';
        $user->save();
        $team = new Team();
        $team->user_id = $user->id;
        $team->name = $user->name;
        $team->personal_team = 1;
        $team->save();
        $role_user = new RoleUser();
        $role = Role::where('name', 'user')->first();
        $role_user->role_id = $role->id;
        $role_user->user_id = $user->id;
        $role_user->save();
    }

}
